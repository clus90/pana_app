//
//  MPPaymentConfirmationViewController.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 3/25/17.
//  Copyright © 2017 Sappito Technologies, C.A. All rights reserved.
//

import UIKit
import SnapKit
import MercadoPagoSDK

class MPPaymentConfirmationViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    var items : [PaymentMethodRow] = []
    // User's saved card
    var selectedCard : Card? = nil
    var selectedPaymentMethod : PaymentMethod? = nil
    var isNewCard = false
    var mercadoPagoConfig = MercadoPagoConfig ()
    
    enum CellType: Int {
        case header
        case cardNumber
        case securityCode
        case footer
        case separator
        case confirmButton
    }
    
    func cellList () -> [CellType]  {
        return [.separator, .header, .cardNumber, .securityCode, .footer, .separator, .confirmButton, .separator]
    }
    
    var cardNumberCell: MPPaymentMethodCell?
    var securityCodeCell: MPSecurityCodeCell?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.allowsSelection = false
        let backgroundImageView = UIImageView (image: UIImage(named: "Background_Login"))
        backgroundImageView.contentMode = .scaleAspectFill
        tableView.backgroundView = backgroundImageView
        let paymentMethodNib = UINib(nibName: "MPPaymentMethodCell", bundle: nil)
        self.tableView.register(paymentMethodNib, forCellReuseIdentifier: "MPPaymentMethodCell")
        self.tableView.register(UINib(nibName: "EmptyViewCell", bundle: nil), forCellReuseIdentifier: "EmptyViewCell")
        self.tableView.register(UINib(nibName: "MPSecurityCodeCell", bundle: nil), forCellReuseIdentifier: "MPSecurityCodeCell")
        self.tableView.register(UINib(nibName: "ButtonCell", bundle: nil), forCellReuseIdentifier: "ButtonCell")
    }
    
    // MARK: - Selectors
    
    func getTransactionToken () {
        if !validateSecurityCode() {
            return
        }
        
        let mercadoPago = MercadoPago (publicKey: mercadoPagoConfig.publicKey)
        if let selectedCard = self.selectedCard, let securityCode = self.securityCodeCell?.securityCodeTextField.text {
            let savedCardToken = SavedCardToken (card: selectedCard, securityCode: securityCode, securityCodeRequired: true)
            if savedCardToken.validate() {
                AppManager.shared.showHUD(self.view, message: localizedStringForKey("loading"))
                mercadoPago.createToken(savedCardToken, success: { (token) in
                    if let token = token {
                        self.submitPaymentWith(token: token)
                    }
                }, failure: { (error) in
                    print ("Error getting token")
                    AppManager.shared.hideHUD(self.view)
                })
            }
        }
    }
    
    // MARK: - Validate
    
    func validateSecurityCode () -> Bool {
        var isValidated = true
        if let securityCodeCell = securityCodeCell {
            if !securityCodeCell.securityCodeTextField.hasText {
                isValidated = false
                securityCodeCell.needsToDisplayError(true)
            } else {
                securityCodeCell.needsToDisplayError(false)
            }
        }
        return isValidated
    }
    
    // MARK: - Submit payment
    
    private func submitPaymentWith (token: Token) {
        if let paymentMethod = self.selectedPaymentMethod {
            PanaAPI.sharedAPI.submitPayment(token: token._id, amount: mercadoPagoConfig.price, isNewCard: isNewCard, paymentMethod: paymentMethod._id, successBlock: { (message) in
                AppManager.shared.hideHUD(self.view)
                self.showSuccessAlert(message)
            }, errorBlock: { (error) in
                AppManager.shared.hideHUD(self.view)
                print ("Error submiting payment:")
                if let error = error?["errors"] as? [String:AnyObject], let message = error["message"] as? String {
                    self.showAlertSingleOption(localizedStringForKey("sorry"), message: message)
                } else {
                    self.showAlertSingleOption(localizedStringForKey("sorry"), message: localizedStringForKey("unable_to_process_payment"))
                }
            })
        }
    }
    
    // MARK: - Alert
    
    func showSuccessAlert (_ message: String) {
        let alert = UIAlertController (title: localizedStringForKey("thanks"), message: message, preferredStyle: UIAlertControllerStyle.alert)
        let action = UIAlertAction (title: localizedStringForKey("accept"), style: .default) { (alert) in
            self.navigationController?.dismiss(animated: true, completion: { 
                AppManager.shared.getUser(completion: nil)
            })
        }
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }

    
    // MARK: - UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellList().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch cellList()[indexPath.row] {
        case .header:
            let cell: EmptyViewCell = tableView.dequeueReusableCell(withIdentifier: "EmptyViewCell", for: indexPath) as! EmptyViewCell
            cell.shouldApplyTopCornerRadius = true
            let view = MPPaymentView ()
            view.priceLabel.text = self.mercadoPagoConfig.getPriceFormated()
            view.monthsLabel.text = String (format: localizedStringForKey("subscription_for"), self.mercadoPagoConfig.months)
            cell.setWith(view: view)
            return cell
        case .footer:
            let cell: EmptyViewCell = tableView.dequeueReusableCell(withIdentifier: "EmptyViewCell", for: indexPath) as! EmptyViewCell
            cell.shouldApplyBottomCornerRadius = true
            let view = MPFooterView ()
            cell.setWith(view: view)
            return cell
        case .cardNumber:
            if let cardNumberCell = cardNumberCell {
                return cardNumberCell
            } else {
                let cell : MPPaymentMethodCell = tableView.dequeueReusableCell(withIdentifier: "MPPaymentMethodCell", for: indexPath) as! MPPaymentMethodCell
                if let selectedCard = self.selectedCard {
                    cell.fillWithCard(selectedCard)
                }
                cardNumberCell = cell
                return cell
            }
        case .securityCode:
            if let securityCodeCell = securityCodeCell {
                return securityCodeCell
            } else {
                let cell : MPSecurityCodeCell = tableView.dequeueReusableCell(withIdentifier: "MPSecurityCodeCell", for: indexPath) as! MPSecurityCodeCell
                if let selectedPaymentMethod = selectedPaymentMethod {
                    cell.fillWithPaymentMethod(selectedPaymentMethod)
                }
                securityCodeCell = cell
                return cell
            }
        case .confirmButton:
            let cell: ButtonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell", for: indexPath) as! ButtonCell
            cell.btn.addTarget(self, action: #selector(self.getTransactionToken), for: .touchUpInside)
            cell.setupWith(title: localizedStringForKey("confirm"))
            cell.btn.titleLabel?.font = AppTheme.fontForThemeElement(.loginButton)
            return cell
        case .separator:
            let cell = UITableViewCell ()
            cell.backgroundColor = .clear
            cell.selectionStyle = .none
            return cell
        }
    }
    
    // MARK: - UITableViewDelegate methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch cellList()[indexPath.row] {
        case .header:
            return 170
        case .footer:
            return 140
        case .cardNumber:
            return 50
        case .securityCode:
            return MPSecurityCodeCell.preferredHeight()
        case .confirmButton:
            return ButtonCell.preferredHeight()
        case .separator:
            return 30
        }
    }

    
}


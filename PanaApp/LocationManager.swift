//
//  LocationManager.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 7/9/16.
//  Copyright © 2016 Sappito Technologies, C.A. All rights reserved.
//

import UIKit
import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager ()
    var currentLocation: CLLocationCoordinate2D?
    
    static let sharedManager = LocationManager()
    
    //This prevents others from using the default '()' initializer for this class.
    fileprivate override init() {
        super.init()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        if CLLocationManager.authorizationStatus() != .notDetermined || CLLocationManager.authorizationStatus() != .denied {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    // MARK: - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let currentCoordinate = locations [locations.count - 1]
        let lat = currentCoordinate.coordinate.latitude
        let lon = currentCoordinate.coordinate.longitude
        self.currentLocation = CLLocationCoordinate2D (latitude: lat, longitude: lon)
        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "didUpdateLocation"), object: nil))
    }
    
}

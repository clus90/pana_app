//
//  ViewController.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 7/7/16.
//  Copyright © 2016 Sappito Technologies, C.A. All rights reserved.
//

import UIKit

class ViewFactory: NSObject {
    
    enum AppView:Int {
        case login
        case signup
        case forgotPassword
        case home
        case detail
        case cancelList
        case profile
        case changePassword
        case menu
        case webView
        case mp_Cards
        case mp_NewCard
        case mp_NewCardController
        case mp_Main
        case mp_Confirmation
    }
    
    class func viewControllerForAppView(_ view:AppView) -> UIViewController {
        
        let viewController:UIViewController?
        
        switch view {
        case .login:
            viewController = UINavigationController(rootViewController: LoginViewController())
            (viewController! as! UINavigationController).interactivePopGestureRecognizer?.delegate = nil
            break
        case .signup:
            let vc = LoginViewController ()
            vc.activeProtocol = .signUp
            viewController = vc
            break
        case .forgotPassword:
            let vc = PasswordChangeViewController ()
            vc.activeProtocol = .forgot
            viewController = vc
            break
        case .home:
            viewController = UINavigationController(rootViewController: HomeViewController())
            break
        case .detail:
            viewController = DetailViewController ()
            break
        case .cancelList:
            viewController = UINavigationController (rootViewController: ListViewController())
        case .profile:
            viewController = UINavigationController (rootViewController: ProfileViewController())
        case .changePassword:
            let vc = PasswordChangeViewController ()
            vc.activeProtocol = .change
            viewController = vc
            break
        case .menu:
            viewController = MenuViewController ()
            break
        case .webView:
            viewController = UINavigationController(rootViewController: WebViewController ())
            break
        case .mp_Cards:
            let vc = MPMainViewController ()
            vc.protocolType = .cards
            viewController = UINavigationController (rootViewController: vc)
            break
        case .mp_NewCardController:
            let vc = MPMainViewController ()
            vc.protocolType = .firstCard
            viewController = UINavigationController (rootViewController: vc)
            break
        case .mp_NewCard:
            let vc = MPMainViewController ()
            vc.protocolType = .newCard
            viewController = vc
            break
        case .mp_Main:
            viewController = UINavigationController(rootViewController: MPMainViewController())
            break
        case .mp_Confirmation:
            viewController = MPPaymentConfirmationViewController ()
            break
        }
        return viewController!
    }
    
    
}



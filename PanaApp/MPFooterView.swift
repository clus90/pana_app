//
//  MPFooterView.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 3/30/17.
//  Copyright © 2017 Sappito Technologies, C.A. All rights reserved.
//

import UIKit

class MPFooterView: UIView {
    
    override init(frame: CGRect) { // for using CustomView in code
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) { // for using CustomView in IB
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    
    
    // MARK: - Init
    
    private func commonInit() {
        if let view = UINib (nibName: "MPFooterView", bundle: Bundle.main).instantiate(withOwner: self, options: nil)[0] as? UIView {
            view.frame = bounds
            view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.addSubview(view)
        }
    }
    
}


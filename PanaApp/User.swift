//
//  User.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 3/17/17.
//  Copyright © 2017 Sappito Technologies, C.A. All rights reserved.
//

import UIKit
import Mantle

class User: BaseEntity {
    
    var id: NSNumber = 0
    var firstName: String = ""
    var lastName: String = ""
    var countryCode: String = ""
    var phone: String = ""
    var document: String = ""
    var documentType: String = ""
    var paymentExpirationDate: String = ""
    var email: String = ""
    var status: Bool = false
    var mercadoPago: Bool = false
    
    override class func jsonKeyPathsByPropertyKey() -> [AnyHashable: Any]! {
        return ["id":"id",
                "firstName":"first_name",
                "lastName":"last_name",
                "countryCode":"call_code",
                "phone":"phone",
                "document":"document",
                "documentType":"type_document",
                "paymentExpirationDate": "expiration_date",
                "email": "email",
                "status": "status",
                "mercadoPago": "mercadopago"]
    }
    
    func getReadableDate () -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        if let date = dateFormatter.date(from: paymentExpirationDate) {
            dateFormatter.dateFormat = "dd-MM-yyyy"
            return dateFormatter.string(from: date)
        }
        return ""
    }

}


//
//  PanaAPI.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 8/16/16.
//  Copyright © 2016 Sappito Technologies, C.A. All rights reserved.
//

import UIKit
import Mantle

class PanaAPI: NSObject {
    
    static let sharedAPI = PanaAPI()
    fileprivate override init() {} //This prevents others from using the default '()' initializer for this class.
    
    func requestWithRequest(_ request:PanaAPIRequest, pathArgs: [String]?, parseClass:AnyClass, completion:@escaping (_ response:AnyObject?) -> (), error: @escaping (_ error:AnyObject?) -> ()) {
        request.requestAPI(pathArgs, completion: { (responseObject) in
            var json: NSDictionary
            if (request.startNode != nil) {
                json = responseObject[request.startNode!] as!  NSDictionary
            } else {
                json = responseObject as! NSDictionary
            }
            do {
                let object:AnyObject = try MTLJSONAdapter.model(of: parseClass, fromJSONDictionary: json as! [AnyHashable: Any]) as AnyObject
                completion(object)
            } catch {
              print ("catch")
            }
        }) { (operation) in 
            error (operation)
        }
    }
    
    func requestWithRequest(_ request:PanaAPIRequest, pathArgs: [String]?, parseClassArray:AnyClass, completion:@escaping (_ response:[AnyObject]?) -> (), error: @escaping (_ error:AnyObject?) -> ()) {
        request.requestAPI(pathArgs, completion: { (responseObject) in
            var jsonArray: [AnyObject]
            if (request.startNode != nil) {
                jsonArray = responseObject[request.startNode!] as!  [AnyObject]
            } else {
                jsonArray = responseObject as! [AnyObject]
            }
            var array:[AnyObject]
            do {
                array = try MTLJSONAdapter.models(of: parseClassArray, fromJSONArray: jsonArray) as [AnyObject]
            } catch {
                array = []
            }
            completion (array)
        }) { (operation) in
            error (operation)
        }
    }
    
    func requestWithRequest(_ request:PanaAPIRequest, pathArgs: [String]?, completion:@escaping (_ response:AnyObject?) -> (), error: @escaping (_ error:AnyObject?) -> ()) {
        request.requestAPI(pathArgs, completion: { (responseObject) in
            var response: AnyObject
            if (request.startNode != nil) {
                response = responseObject[request.startNode!] as AnyObject
            } else {
                response = responseObject
            }
            completion (response)
        }) { (operation) in
            error (operation)
        }
    }
}


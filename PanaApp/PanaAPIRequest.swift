//
//  PanaAPIRequest.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 8/16/16.
//  Copyright © 2016 Sappito Technologies, C.A. All rights reserved.
//

import UIKit
import AFNetworking

class PanaAPIRequest: NSObject {
    
    let baseURL = "http://www.panatech.io/"
    
    enum APIError: Error {
        case wrongParameters
    }
    
    enum APIService {
        case login
        case register
        case forgotPassword
        case updateAccessToken
        case getProfile
        case updateProfile
        case changePassword
        case requestService
        case getServiceInfo(id:String)
        case endService(id:String)
        case getCancelingReasons
        case cancelService(id:String)
        case mercadopagoConfig
        case getUserCardsMP
        case submintPayment
    }
    
    enum RequestMethod {
        case post
        case get
        case patch
        case delete
    }
    
    var service: APIService
    var parameters:AnyObject?
    var startNode: String?
    var manager = AFHTTPRequestOperationManager ()
    let securityPolicy = AFSecurityPolicy(pinningMode: AFSSLPinningMode.none)
    
    init(service:APIService, parameters:AnyObject?, startNode: String?)  {
        self.service = service
        self.parameters = parameters
        self.startNode = startNode
        self.manager.securityPolicy = self.securityPolicy
        self.manager.securityPolicy.allowInvalidCertificates = true
        self.manager.requestSerializer = AFJSONRequestSerializer ()
        self.manager.responseSerializer.acceptableContentTypes = NSSet(objects: "application/json", "text/html") as Set<NSObject>
    }
    
    func requestAPI (path: String, completion:@escaping (_ responseObject: AnyObject) -> Void, error:@escaping (_ operation: AnyObject?) -> Void) {
        self.apiGET(path, success: { (responseObject) in
            completion (responseObject as AnyObject)
            }, errorBlock: { (operation) in
                error (operation as AnyObject?)
        })
    }
    
    func requestAPI (_ pathArgs: [String]?, completion:@escaping (_ responseObject: AnyObject) -> Void, error:@escaping (_ operation: AnyObject?) -> Void) {
        let request: (path:String, method:RequestMethod) = self.path(pathArgs)
        switch request.method {
        case .get:
            self.apiGET(request.path, success: { (responseObject) in
                completion (responseObject as AnyObject)
                }, errorBlock: { (operation) in
                    error (operation as AnyObject?)
            })
            break
        case .post:
            self.apiPOST(request.path, success: { (responseObject) in
                completion (responseObject as AnyObject)
                }, errorBlock: { (operation) in
                    error (operation as AnyObject?)
            })
            break
        case .patch:
            self.apiPATCH(request.path, success: { (responseObject) in
                completion (responseObject as AnyObject)
            }, errorBlock: { (operation) in
                error (operation as AnyObject?)
            })
            break
        case .delete:
            self.apiDelete(request.path, success: { (responseObject) in
                completion (responseObject as AnyObject)
                }, errorBlock: { (operation) in
                    error (operation as AnyObject?)
            })
            break
        }
    }
    
    fileprivate func path(_ pathArgs: [String]?) -> (String, RequestMethod) {
        var path:String = ""
        var requestMethod: RequestMethod = RequestMethod.get
        switch self.service {
        case .login, .updateAccessToken:
            path = "api/oauth/access_token"
            requestMethod = RequestMethod.post
        case .register:
            path = "api/register"
            requestMethod = RequestMethod.post
        case .forgotPassword:
            path = "api/reset_password"
            requestMethod = RequestMethod.post
        case .getProfile:
            path = "api/profile"
            requestMethod = RequestMethod.get
        case .updateProfile:
            path = "api/profile"
            requestMethod = RequestMethod.patch
        case .changePassword:
            path = "api/profile/password"
            requestMethod = RequestMethod.patch
        case .requestService:
            path = "api/petitions"
            requestMethod = RequestMethod.post
        case .getServiceInfo (let id):
            path = "api/petitions/\(id)/"
            requestMethod = RequestMethod.get
        case .endService(let id):
            path = "api/petitions/\(id)"
            requestMethod = RequestMethod.patch
        case .getCancelingReasons:
            path = "api/reasons"
            requestMethod = RequestMethod.get
        case .cancelService(let id):
            path = "api/petitions/\(id)/cancel"
            requestMethod = RequestMethod.post
        case .mercadopagoConfig:
            path = "api/config"
            requestMethod = RequestMethod.get
        case .getUserCardsMP:
            path = "api/tdc"
            requestMethod = RequestMethod.get
        case .submintPayment:
            path = "api/subscribe"
            requestMethod = RequestMethod.post
        }
        return (baseURL+path, requestMethod)
    }
    
    fileprivate func apiGET (_ path: String, success: @escaping (_ responseObject: Any) -> Void, errorBlock: @escaping (_ operation: Any?) -> Void) {
        self.manager.get(path, parameters: self.parameters, success: { (operation, responseObject) in
            success (responseObject)
        }) { (operation, error) in
            errorBlock (operation?.responseObject)
        }
    }
    
    fileprivate func apiPOST (_ path: String, success: @escaping (_ responseObject: Any) -> Void, errorBlock: @escaping (_ operation: Any?) -> Void) {
        self.manager.post(path, parameters: self.parameters, success: { (operation, responseObject) in
            success (responseObject)
        }) { (operation, error) in
            print ("ERROR: \(operation?.responseString)")
            errorBlock (operation?.responseObject)
        }
    }
    
    fileprivate func apiPATCH (_ path: String, success: @escaping (_ responseObject: Any) -> Void, errorBlock: @escaping (_ operation: Any?) -> Void) {
        self.manager.patch(path, parameters: self.parameters, success: { (operation, responseObject) in
            success (responseObject)
        }) { (operation, error) in
            print ("ERROR: \(operation?.responseString)")
            errorBlock (operation?.responseObject)
        }
    }
    
    fileprivate func apiDelete (_ path: String, success: @escaping (_ responseObject: Any) -> Void, errorBlock: @escaping (_ operation: Any?) -> Void) {
        self.manager.delete(path, parameters: self.parameters, success: { (operations, responseObject) in
            success (responseObject)
        }) { (operation, error) in
            errorBlock (operation?.responseObject)
        }
    }
    
    fileprivate func checkParameters (_ pathArgs: [String]?, expectedCount: Int) throws -> Void {
        if let pathArgs = pathArgs {
            if pathArgs.count != expectedCount {
                throw APIError.wrongParameters
            }
        }
    }
    
}

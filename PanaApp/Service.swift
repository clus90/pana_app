//
//  Service.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 8/16/16.
//  Copyright © 2016 Sappito Technologies, C.A. All rights reserved.
//

import UIKit
import Mantle

class Service: BaseEntity {
    
    var id: NSNumber = 0
    var status: String = ""
    var finished: Bool = false
    var canceled: Bool = false
    var latitude: String = ""
    var longitude: String = ""
    
    override class func jsonKeyPathsByPropertyKey() -> [AnyHashable: Any]! {
        return ["id":"id",
                "status":"status",
                "finished":"finished",
                "canceled":"canceled",
                "latitude":"latitude",
                "longitude":"longitude"]
    }
    
}


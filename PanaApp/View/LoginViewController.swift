//
//  LoginViewController.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 7/7/16.
//  Copyright © 2016 Sappito Technologies, C.A. All rights reserved.
//

import UIKit
import CountryPicker

class LoginViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    enum LoginCellType: Int {
        case topSeparator
        case smallSeparator
        case firstName
        case lastName
        case email
        case phone
        case id
        case password
        case confirmPassword
        case loginButton
        case signupButton
        case notMemberLabel
        case termsAgreement
        case register
        case forgotPasswordButton
    }
    
    enum LoginActiveProtocol: Int {
        case signUp
        case login
    }
    
    // Set default protocol to SignUp
    var activeProtocol:LoginActiveProtocol = .login
    
    var firstNameTextFieldCell:TextFieldCell?
    var lastNameTextFieldCell:TextFieldCell?
    var emailTextFieldCell:TextFieldCell?
    var phoneTextFieldCell: DoubleTextFieldCell?
    var idTextFieldCell: DoubleTextFieldCell?
    var passwordTextFieldCell:TextFieldCell?
    var confirmPasswordTextFieldCell:TextFieldCell?
    var countryCodePicker = CountryPicker ()
    var termsAgreed: Bool = false
    let idTypePicker = UIPickerView ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.tintColor = .white
        setupCountryPicker()
        setupPickerView()
        loadTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func needsNavigationBarTransparent() -> Bool {
        return true
    }
    
    override func needsLogoOnNavBar() -> Bool {
        return activeProtocol == .signUp
    }
    
    func loadTableView() {
        let backgroundImageView = UIImageView (image: UIImage(named: "Background_Login"))
        backgroundImageView.contentMode = .scaleAspectFill
        tableView.backgroundView = backgroundImageView
        tableView.register(UINib(nibName: "CenteredImageCell", bundle: nil), forCellReuseIdentifier: "CenteredImageCell")
        tableView.register(UINib(nibName: "TextFieldCell", bundle: nil), forCellReuseIdentifier: "TextFieldCell")
        tableView.register(UINib(nibName: "DoubleTextFieldCell", bundle: nil), forCellReuseIdentifier: "DoubleTextFieldCell")
        tableView.register(UINib(nibName: "ButtonCell", bundle: nil), forCellReuseIdentifier: "ButtonCell")
        tableView.register(UINib(nibName: "LabelCell", bundle: nil), forCellReuseIdentifier: "LabelCell")
        tableView.register(UINib(nibName: "AttributedTextCell", bundle: nil), forCellReuseIdentifier: "AttributedTextCell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.allowsSelection = false
    }
    
    func setupCountryPicker () {
        countryCodePicker.countryPickerDelegate = self
        countryCodePicker.setCountry("VE")
    }
    
    func setupPickerView () {
        idTypePicker.backgroundColor = UIColor.lightGray
        idTypePicker.delegate = self
        idTypePicker.dataSource = self
    }

    
    func loginCellList() -> [LoginCellType] {
        switch self.activeProtocol {
        case .signUp:
            return [.smallSeparator, .firstName, .lastName, .email, .phone, .id, .password, .confirmPassword, .termsAgreement, .signupButton]
        case .login:
            return [.topSeparator, .smallSeparator, .email, .password, .forgotPasswordButton, .loginButton, .notMemberLabel, .register]
        }
    }
    
    // MARK: - Actions and Selectors
    
    func forgotPasswordTapped(_ sender:UIButton) {
        self.pushToView(.forgotPassword, completion: nil)
    }
    
    func loginButtonTapped(_ sender: UIButton?) {
        if (!validateForm()) {
            return
        }
        if let email = self.emailTextFieldCell?.textField.text, let password = self.passwordTextFieldCell?.textField.text {
            loginWithCredentialas(email: email, password: password)
        }

    }
    
    func signUpButtonTapped (_ sender: UIButton) {
        switch activeProtocol {
        case .signUp:
            if (!validateForm()) {
                return
            }
            if let firstName = firstNameTextFieldCell?.textField.text, let lastName = lastNameTextFieldCell?.textField.text, let email = emailTextFieldCell?.textField.text, let countryCode = phoneTextFieldCell?.leftTextField.text, let phone = phoneTextFieldCell?.rightTextField.text, let idType = idTextFieldCell?.leftTextField.text, let id = idTextFieldCell?.rightTextField.text, let password = passwordTextFieldCell?.textField.text {
                
                if termsAgreed {
                    AppManager.shared.showHUD(self.view, message: localizedStringForKey("loading"))
                    PanaAPI.sharedAPI.registerUser(firstName, lastName: lastName, email: email, countryCode: countryCode, phone: phone, idType: idType, id: id, password: password, successBlock: {
                        AppManager.shared.hideHUD(self.view)
                        self.loginWithCredentialas(email: email, password: password)
                    }, errorBlock: { (error) in
                        AppManager.shared.hideHUD(self.view)
                        self.showAlertSingleOption(localizedStringForKey("sorry"), message: localizedStringForKey("wrongRegisterMessage"))
                    })
                } else {
                    showAlertSingleOption(localizedStringForKey("sorry"), message: localizedStringForKey("must_accept_terms"))
                }
                
            }
            break
        case .login:
            pushToView(.signup, completion: nil)
            break
        }
    }
    
    func termsButtonTapped (_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        termsAgreed = sender.isSelected
    }
    
    func loginWithCredentialas (email: String, password: String) {
        AppManager.shared.showHUD(self.view, message: localizedStringForKey("loading"))
        PanaAPI.sharedAPI.loginWithUsername(email, password: password, successBlock: {
            AppManager.shared.hideHUD(self.view)
            LocationManager.sharedManager.locationManager.startUpdatingLocation()
            AppManager.shared.setCenterView(ViewFactory.AppView.home, setup: nil)
        }, errorBlock: { (error) in
            AppManager.shared.hideHUD(self.view)
            self.showAlertSingleOption(localizedStringForKey("sorry"), message: localizedStringForKey("wrongLoginMessage"))
        })
    }
    
    // MARK: - Validation Methods
    
    func validateForm () -> Bool {
        switch activeProtocol {
        case .login:
            return validateForLogin()
        case .signUp:
            return validateForSignUp()
        }    }
    
    func validateForLogin () -> Bool {
        var isValidated: Bool = true
        if (!emailTextFieldCell!.hasText()) {
            isValidated = false
            emailTextFieldCell!.needsToDisplayError(true)
        } else {
            emailTextFieldCell!.needsToDisplayError(false)
        }
        if (!passwordTextFieldCell!.hasText()) {
            isValidated = false
            passwordTextFieldCell!.needsToDisplayError(true)
        } else {
            passwordTextFieldCell!.needsToDisplayError(false)
        }
        return isValidated
    }
    
    func validateForSignUp () -> Bool {
        var isValidated: Bool = true
        isValidated = isValidated && isEmailTextFieldValid()
        
        if (!firstNameTextFieldCell!.hasText()) {
            isValidated = false
            firstNameTextFieldCell!.needsToDisplayError(true)
        } else {
            firstNameTextFieldCell!.needsToDisplayError(false)
        }
        if (!lastNameTextFieldCell!.hasText()) {
            isValidated = false
            lastNameTextFieldCell!.needsToDisplayError(true)
        } else {
            lastNameTextFieldCell!.needsToDisplayError(false)
        }
        if (!phoneTextFieldCell!.hasText()) {
            isValidated = false
            phoneTextFieldCell!.needsToDisplayError(true)
        } else {
            if let length = phoneTextFieldCell?.rightTextField.text?.length, length != 10 {
                isValidated = false
                phoneTextFieldCell!.needsToDisplayError(true)
            } else {
                phoneTextFieldCell!.needsToDisplayError(false)
            }
        }
        if (!idTextFieldCell!.hasText()) {
            isValidated = false
            idTextFieldCell!.needsToDisplayError(true)
        } else {
            if let length = idTextFieldCell?.rightTextField.text?.length, length > 8 {
                isValidated = false
                idTextFieldCell!.needsToDisplayError(true)
            } else {
                idTextFieldCell!.needsToDisplayError(false)
            }
        }
        
        isValidated = isValidated && isPasswordTextFieldValid()
        return isValidated
    }
    
    func isEmailTextFieldValid() -> Bool {
        
        var isValidated:Bool = true
        
        if (!emailTextFieldCell!.hasText() || !emailTextFieldCell!.isEmailTextValid()) {
            isValidated = false
            emailTextFieldCell!.needsToDisplayError(true)
        } else {
            emailTextFieldCell!.needsToDisplayError(false)
        }
        
        return isValidated
    }
    
    func isPasswordTextFieldValid() -> Bool {
        var isValidated:Bool = true
        
        if (!passwordTextFieldCell!.hasText()) {
            isValidated = false
            passwordTextFieldCell!.needsToDisplayError(true)
            return isValidated
        } else {
            passwordTextFieldCell!.needsToDisplayError(false)
        }
        
        if (activeProtocol == .signUp) {
            if (passwordTextFieldCell!.textField.text!.characters.count < 8 ) {
                showAlertSingleOption(localizedStringForKey("sorry"), message: localizedStringForKey("password_length_alert"))
                isValidated = false
            }
            if (passwordTextFieldCell!.textField.text != confirmPasswordTextFieldCell!.textField.text) || (!passwordTextFieldCell!.isNoSpacesValid()) {
                isValidated = false
                passwordTextFieldCell!.needsToDisplayError(true)
                confirmPasswordTextFieldCell!.needsToDisplayError(true)
            } else {
                passwordTextFieldCell!.needsToDisplayError(false)
                confirmPasswordTextFieldCell!.needsToDisplayError(false)
            }
        }
        
        return isValidated
    }

    
    // MARK: - UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return loginCellList().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType:LoginCellType = loginCellList()[(indexPath as NSIndexPath).row]
        
        switch cellType {
        case .topSeparator:
            let cell: CenteredImageCell = tableView.dequeueReusableCell(withIdentifier: "CenteredImageCell", for: indexPath) as! CenteredImageCell
            cell.imgView.image =  UIImage (named: "Logo_Login")
            return cell
        case .firstName:
            let cell: TextFieldCell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.placeholder = localizedStringForKey("first_name")
            cell.textField.autocapitalizationType = .words
            cell.textField.returnKeyType = UIReturnKeyType.done
            cell.textField.delegate = self
            cell.shouldApplyTopCornerRadius = activeProtocol == .signUp
            cell.imgView.image = UIImage (named: "Pencil_Image")
            firstNameTextFieldCell = cell
            return cell
        case .lastName:
            let cell: TextFieldCell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.placeholder = localizedStringForKey("last_name")
            cell.textField.autocapitalizationType = .words
            cell.textField.returnKeyType = UIReturnKeyType.done
            cell.textField.delegate = self
            cell.imgView.image = UIImage (named: "Pencil_Image")
            lastNameTextFieldCell = cell
            return cell
        case .email:
            let cell: TextFieldCell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.placeholder = localizedStringForKey("email")
            cell.textField.keyboardType = .emailAddress
            cell.textField.autocapitalizationType = .none
            cell.textField.returnKeyType = UIReturnKeyType.done
            cell.textField.delegate = self
            cell.shouldApplyTopCornerRadius = activeProtocol == .login
            cell.imgView.image = UIImage (named: "Email_Image")
            emailTextFieldCell = cell
            return cell
        case .phone:
            let cell: DoubleTextFieldCell = tableView.dequeueReusableCell(withIdentifier: "DoubleTextFieldCell", for: indexPath) as! DoubleTextFieldCell
            cell.leftTextField.text = "+58"
            cell.rightTextField.placeholder = localizedStringForKey("phone_number")
            cell.rightTextField.keyboardType = .phonePad
            cell.rightTextField.delegate = self
            cell.leftTextField.inputView = countryCodePicker
            cell.imgView.image = UIImage (named: "Phone_Icon")
            phoneTextFieldCell = cell
            return cell
        case .id:
            let cell: DoubleTextFieldCell = tableView.dequeueReusableCell(withIdentifier: "DoubleTextFieldCell", for: indexPath) as! DoubleTextFieldCell
            cell.rightTextField.placeholder = localizedStringForKey("id")
            cell.rightTextField.keyboardType = .numberPad
            cell.rightTextField.returnKeyType = UIReturnKeyType.done
            cell.rightTextField.delegate = self
            cell.leftTextField.text = kIdTypes[0]
            cell.leftTextField.inputView = idTypePicker
            cell.imgView.image = UIImage (named: "Pencil_Image")
            idTextFieldCell = cell
            return cell
        case .password:
            let cell: TextFieldCell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.placeholder = localizedStringForKey("password")
            cell.textField.isSecureTextEntry = true
            cell.textField.returnKeyType = self.activeProtocol == .login ? UIReturnKeyType.go : UIReturnKeyType.done
            cell.textField.delegate = self
            cell.shouldApplyBottomCornerRadius = activeProtocol == .login
            cell.shouldHideUnderLineView = activeProtocol == .login
            cell.imgView.image = UIImage (named: "Password_Image")
            passwordTextFieldCell = cell
            return cell
        case .confirmPassword:
            let cell: TextFieldCell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.placeholder = localizedStringForKey("confirm_password")
            cell.textField.isSecureTextEntry = true
            cell.textField.returnKeyType = UIReturnKeyType.done
            cell.textField.delegate = self
            cell.shouldApplyBottomCornerRadius = activeProtocol == .signUp
            cell.shouldHideUnderLineView = activeProtocol == .signUp
            cell.imgView.image = UIImage (named: "Password_Image")
            confirmPasswordTextFieldCell = cell
            return cell
        case .loginButton:
            let cell: ButtonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell", for: indexPath) as! ButtonCell
            cell.btn.addTarget(self, action: #selector(LoginViewController.loginButtonTapped(_:)), for: .touchUpInside)
            cell.setupWith(title: localizedStringForKey("login"))
            cell.btn.titleLabel?.font = AppTheme.fontForThemeElement(.loginButton)
            return cell
        case .signupButton:
            let cell: ButtonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell", for: indexPath) as! ButtonCell
            cell.btn.addTarget(self, action: #selector(LoginViewController.signUpButtonTapped(_:)), for: .touchUpInside)
            cell.setupWith(title: localizedStringForKey("sign_up"))
            cell.btn.titleLabel?.font = AppTheme.fontForThemeElement(.loginButton)
            return cell
        case .register:
            let cell: ButtonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell", for: indexPath) as! ButtonCell
            cell.btn.addTarget(self, action: #selector(LoginViewController.signUpButtonTapped(_:)), for: .touchUpInside)
            cell.setupUnderlinedWith(title: localizedStringForKey("registerHere"))
            cell.btn.titleLabel?.font = AppTheme.fontForThemeElement(.loginButton)
            return cell
        case .notMemberLabel:
            let cell: LabelCell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath) as! LabelCell
            cell.label.text = localizedStringForKey("notAMemberYet")
            cell.label.textColor = AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.loginLabel)
            cell.label.textAlignment = .center
            cell.label.font = AppTheme.fontForThemeElement(.loginButton)
            cell.setForClearMode()
            return cell
        case .termsAgreement:
            let cell:AttributedTextCell = tableView.dequeueReusableCell(withIdentifier: "AttributedTextCell", for: indexPath) as! AttributedTextCell
            cell.setAttributedText(localizedStringForKey("terms_agreement"))
            cell.btn.addTarget(self, action: #selector(self.termsButtonTapped(_:)), for: .touchUpInside)
            return cell
        case .forgotPasswordButton:
            let cell: ButtonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell", for: indexPath) as! ButtonCell
            cell.btn.addTarget(self, action: #selector(LoginViewController.forgotPasswordTapped(_:)), for: .touchUpInside)
            cell.setupUnderlinedWith(title: localizedStringForKey("forgotPassword"))
            return cell
        case .smallSeparator:
            let cell = UITableViewCell ()
            cell.backgroundColor = .clear
            return cell
        }
    }
    
    // MARK: - UITableViewDelegate methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellType:LoginCellType = self.loginCellList()[(indexPath as NSIndexPath).row]
        
        switch cellType {
        case .loginButton, .signupButton:
            return ButtonCell.preferredHeight()
        case .topSeparator:
            let height = UIScreen.main.bounds.height
            return (height * 0.3)
        case .smallSeparator, .register:
            return 30
        case .email, .password, .firstName, .lastName, .phone, .id, .confirmPassword:
            return TextFieldCell.preferredHeight()
        case .termsAgreement:
            return 60
        case .forgotPasswordButton, .notMemberLabel:
            return 40
        }
    }
    
    
    // MARK: - UITextViewDelegate methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if activeProtocol == .login && textField == passwordTextFieldCell?.textField {
            hideKeyboard()
            loginButtonTapped(nil)
            return true
        }
        hideKeyboard()
        return true
    }
}

// MARK: - CountryPicker delegate

extension LoginViewController: CountryPickerDelegate {
    
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        if let textField = phoneTextFieldCell?.leftTextField {
            textField.text = phoneCode
        }
    }
    
}

// MARK: - PickerView Delegate and DataSource

extension LoginViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return kIdTypes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerView.reloadAllComponents()
        if let textField = idTextFieldCell?.leftTextField {
            textField.text = kIdTypes[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let color = (row == pickerView.selectedRow(inComponent: component)) ? UIColor.green : UIColor.black
        return NSAttributedString(string: kIdTypes[row], attributes: [NSForegroundColorAttributeName: color])
    }
}


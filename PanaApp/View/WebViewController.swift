//
//  WebViewController.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 7/22/16.
//  Copyright © 2016 Sappito Technologies, C.A. All rights reserved.
//

import UIKit

class WebViewController: BaseViewController {
    
    var webView = UIWebView ()
    var urlString = String ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupWebView ()
    }
    
    func setupWebView () {
        self.webView.delegate = self
        
        let cancelButton = UIBarButtonItem (image: UIImage(named: "Cancel_Icon"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.closeButtonTapped))
        self.navigationItem.leftBarButtonItem = cancelButton
        
        let toolbar = UIToolbar ()
        toolbar.isOpaque = false
        toolbar.barTintColor = AppTheme.backgroundColorForThemeElement(AppTheme.AppThemeElement.appSecundaryColor)
        toolbar.tintColor = AppTheme.backgroundColorForThemeElement(AppTheme.AppThemeElement.appMainColor)
        let backButton = UIBarButtonItem (barButtonSystemItem: UIBarButtonSystemItem.rewind, target: self, action: #selector(self.backButtonTapped))
        let flexibleSpace = UIBarButtonItem (barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil)
        let forwardButton = UIBarButtonItem (barButtonSystemItem: UIBarButtonSystemItem.fastForward, target: self, action: #selector(self.forwardButtonTapped))
        let refreshButton = UIBarButtonItem (barButtonSystemItem: UIBarButtonSystemItem.refresh, target: self, action: #selector(self.refreshButtonTapped))
        toolbar.setItems([backButton,flexibleSpace,refreshButton,flexibleSpace,forwardButton], animated: false)
        
        self.view.addSubview(self.webView)
        self.view.addSubview(toolbar)
        
        self.webView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        toolbar.snp.makeConstraints { (make) in
            make.left.equalTo(self.view)
            make.bottom.equalTo(self.view)
            make.right.equalTo(self.view)
        }
        
    }
    
    func deleteCookies () {
        let storage = HTTPCookieStorage.shared
        if let cookies = storage.cookies {
            for cookie in cookies {
                storage.deleteCookie(cookie)
            }
        }
    }
    
    // MARK: - Actions
    
    func backButtonTapped() {
        self.webView.goBack()
    }
    
    func forwardButtonTapped() {
        self.webView.goForward()
    }
    
    func refreshButtonTapped() {
        self.webView.reload()
    }
    
    func closeButtonTapped () {
        self.deleteCookies()
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}

extension WebViewController: UIWebViewDelegate {
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if request.url?.absoluteString == "http://rav.evenseg.com/billing" {
            self.closeButtonTapped()
            NotificationCenter.default.post(Notification (name: Notification.Name(rawValue: "registrationFinished")))
            return false
        }
        return true
     }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        AppManager.shared.showHUD(self.view, message: localizedStringForKey ("loading"))
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        AppManager.shared.hideHUD(self.view)
    }
}


//
//  BaseViewController.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 7/7/16.
//  Copyright © 2016 Sappito Technologies, C.A. All rights reserved.
//

import UIKit
import SnapKit

class BaseViewController: UIViewController, UIGestureRecognizerDelegate {
    
    private var _tableView:UITableView?
    var tableView:UITableView {
        get {
            if (_tableView == nil) {
                _tableView = UITableView(frame: view.frame, style: UITableViewStyle.plain)
                _tableView!.backgroundColor = UIColor.black
                _tableView!.separatorStyle = UITableViewCellSeparatorStyle.none
                _tableView!.frame = view.frame
            }
            return _tableView!
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            make.edges.equalTo(view)
        }
        self.setupView()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setupView() {
        self.edgesForExtendedLayout = UIRectEdge.all
        
        if (self.navigationController != nil && needsNavigationBarTransparent()) {
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = true
        } else {
            self.navigationController?.navigationBar.isTranslucent = false
            self.navigationController?.navigationBar.barTintColor = .black
            self.navigationController?.view.backgroundColor = UIColor.black
        }
        if needsLogoOnNavBar() {
            let imageView = UIImageView (image: UIImage (named: "Black_Icon"))
            imageView.contentMode = .scaleAspectFit
            let titleView = UIView (frame: CGRect (x: 0,y: 0, width: 40, height: 40))
            imageView.frame = titleView.bounds
            titleView.addSubview(imageView)
            self.navigationItem.titleView = titleView
        }
        
        self.view.backgroundColor = UIColor.white
        
        let endEditingGestureRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BaseViewController.hideKeyboard))
        endEditingGestureRecognizer.numberOfTapsRequired = 1
        endEditingGestureRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(endEditingGestureRecognizer)
        
        if (self.needsMenuBarButton()) {
            let menuButton = UIBarButtonItem (image: UIImage(named: "Menu_Icon"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.menuButtonTapped))
            self.navigationItem.leftBarButtonItem = menuButton
        }
        
    }
    
    func hideKeyboard() {
        self.view.endEditing(false)
    }
    
    func needsNavigationBarTransparent() -> Bool {
        return false
    }
    
    func needsLogoOnNavBar () -> Bool {
        return true
    }
    
    func pushToView(_ view:ViewFactory.AppView, completion:((_ viewController:UIViewController)->())?) {
        let viewController:UIViewController = ViewFactory.viewControllerForAppView(view)
        self.navigationController?.pushViewController(viewController, animated: true)
        
        if (completion != nil) {
            completion!(viewController)
        }
    }
    
    func presentModalView(_ view:ViewFactory.AppView, completion:((_ viewController:UIViewController)->())?) {
        let viewController:UIViewController = ViewFactory.viewControllerForAppView(view)
        self.navigationController?.present(viewController, animated: true, completion: {
            if (completion != nil) {
                completion!(viewController)
            }
        })
    }
    
    func showAlertSingleOption (_ title: String, message: String) {
        let alert = UIAlertController (title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let action = UIAlertAction (title: localizedStringForKey("accept"), style: UIAlertActionStyle.default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    func menuButtonTapped() {
        AppManager.shared.toggleMenu()
    }
    
    func callOperationCenter () {
        let url = NSURL(string: kOperationCenterPhone)!
        UIApplication.shared.openURL(url as URL)
    }
    
    func needsMenuBarButton () -> Bool {
        return false
    }
    
//    func handleError(error:NSError) {
//        AppManager.sharedManager.showErrorHUD()
//    }
    
}

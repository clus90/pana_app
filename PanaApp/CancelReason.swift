//
//  Reason.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 3/18/17.
//  Copyright © 2017 Sappito Technologies, C.A. All rights reserved.
//

import UIKit
import Mantle

class CancelReason: BaseEntity {
    
    var id: NSNumber = 0
    var reason: String = ""
    
    override class func jsonKeyPathsByPropertyKey() -> [AnyHashable: Any]! {
        return ["id":"id",
                "reason":"reason"]
    }
    
}

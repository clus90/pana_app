//
//  DetailViewController.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 7/10/16.
//  Copyright © 2016 Sappito Technologies, C.A. All rights reserved.
//

import UIKit
import GoogleMaps

class DetailViewController: BaseViewController {
    
    var mapView = GMSMapView ()
    var pinImageView = UIImageView (image: UIImage (named: "Map_Pin"))
    let topContainerView = UIView ()
    var bottomContainerView = UIView ()
    var pinCoordinate: CLLocationCoordinate2D?
    var currentStatusLabel: UILabel = UILabel ()
    var messageLabel = UILabel ()
    var secondMessageLabel = UILabel ()
    var currentService: Service? {
        get {
            return AppManager.shared.currentService
        }
    }
    
    var isViewAnimating: Bool = false
    
    override func viewDidLoad() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.startPinAnimation), name: NSNotification.Name(rawValue: "didBecameActive"), object: nil)
        Timer.scheduledTimer(timeInterval: 20.0, target: self, selector: #selector(self.checkServiceNews), userInfo: nil, repeats: true)
        self.setupDetailView ()
        self.setupMap()
        self.checkServiceNews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkServiceNews()
    }
    
    // MARK: - Setup
    
    func setupDetailView () {
        
        self.currentStatusLabel.textColor = AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.appMainColor)
        self.currentStatusLabel.font = AppTheme.fontForThemeElement(AppTheme.AppThemeElement.detailMessage)
        self.currentStatusLabel.alpha = 0.0
        self.currentStatusLabel.adjustsFontSizeToFitWidth = true
        self.currentStatusLabel.minimumScaleFactor = 0.8
        self.currentStatusLabel.textAlignment = .center
        self.currentStatusLabel.numberOfLines = 2
        
        self.topContainerView.backgroundColor = AppTheme.backgroundColorForThemeElement(AppTheme.AppThemeElement.appSecundaryColor)
        self.topContainerView.layer.cornerRadius = 5
        self.addShadowToView(topContainerView)
        
        
        self.bottomContainerView.backgroundColor = AppTheme.backgroundColorForThemeElement(AppTheme.AppThemeElement.appSecundaryColor)
        self.bottomContainerView.layer.cornerRadius = 5
        self.addShadowToView(self.bottomContainerView)
        
        let callButton = UIBarButtonItem (image: UIImage(named: "Phone_Icon"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.callButtonTapped))
        self.navigationItem.leftBarButtonItem = callButton
        
        let cancelButton = UIBarButtonItem (image: UIImage(named: "Cancel_Icon"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelButtonTapped))
        self.navigationItem.rightBarButtonItem = cancelButton
        
        
        self.messageLabel.text = localizedStringForKey("warningMessage")
        self.messageLabel.textColor = AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.detailMessage)
        self.messageLabel.font = AppTheme.fontForThemeElement(AppTheme.AppThemeElement.statusTitle)
        self.messageLabel.numberOfLines = 0
        
        self.secondMessageLabel.text = localizedStringForKey("detailMessage")
        self.secondMessageLabel.textColor = AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.detailMessage)
        self.secondMessageLabel.font = AppTheme.fontForThemeElement(AppTheme.AppThemeElement.statusTitle)
        self.secondMessageLabel.numberOfLines = 0
        self.secondMessageLabel.alpha = 0.0
        
        let statusTitleLabel = UILabel ()
        statusTitleLabel.text = localizedStringForKey("serviceStatus")
        statusTitleLabel.textColor = AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.detailMessage)
        statusTitleLabel.font = AppTheme.fontForThemeElement(AppTheme.AppThemeElement.statusTitle)
        
        let warningImageView = UIImageView (image: UIImage (named: "warning_icon"))
        
        self.pinImageView.contentMode = .scaleAspectFit
        
        self.view.addSubview(self.mapView)
        self.view.addSubview(self.topContainerView)
        self.view.addSubview(self.bottomContainerView)
        self.view.addSubview(self.pinImageView)
        topContainerView.addSubview(warningImageView)
        topContainerView.addSubview(self.messageLabel)
        topContainerView.addSubview(self.secondMessageLabel)
        self.bottomContainerView.addSubview(statusTitleLabel)
        self.bottomContainerView.addSubview(self.currentStatusLabel)
        
        self.mapView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        self.pinImageView.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.view).offset(-52)
            make.centerX.equalTo(self.view).offset(-22)
            make.size.equalTo(50)
        }
        
        self.topContainerView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view).offset(20)
            make.centerX.equalTo(self.view)
            make.height.equalTo(50)
        }
        
        warningImageView.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.topContainerView)
            make.left.equalTo(self.topContainerView.snp.left).offset(20)
            make.height.equalTo(20)
            make.width.equalTo(23)
        }
        
        self.messageLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.topContainerView)
            make.left.equalTo(warningImageView.snp.right).offset(20)
            make.right.equalTo(self.topContainerView.snp.right).offset(-20)
        }
        
        self.secondMessageLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.topContainerView)
            make.left.equalTo(warningImageView.snp.right).offset(20)
            make.right.equalTo(self.topContainerView.snp.right).offset(-20)
        }
        
        self.bottomContainerView.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.view).offset(-20)
            make.left.equalTo(self.view).offset(20)
            make.right.equalTo(self.view).offset(-20)
            make.height.equalTo(50)
        }
        
        statusTitleLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.bottomContainerView)
            make.left.equalTo(self.bottomContainerView).offset(15)
            make.width.equalTo(140)
        }
        
        self.currentStatusLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.bottomContainerView)
            make.left.equalTo(statusTitleLabel.snp.right).offset(10)
            make.right.equalTo(self.bottomContainerView).offset(-15)
        }
        
    }
    
    func setupMap () {
        if let coordinate = self.pinCoordinate {
            self.mapView.camera = GMSCameraPosition(target: coordinate, zoom: 4, bearing: 0, viewingAngle: 0)
            self.mapView.padding = UIEdgeInsets (top: 0, left: 0, bottom: 70, right: 0)
            self.mapView.isUserInteractionEnabled = false
            delay(seconds: 0.5, completion: {
                let zoomIn = GMSCameraUpdate.zoom(to: 16)
                self.mapView.animate(with: zoomIn)
                self.startTopAnimation()
                self.startPinAnimation()
            })
        }
    }
    
    func addShadowToView (_ view: UIView) {
        view.layer.shadowColor = AppTheme.backgroundColorForThemeElement(AppTheme.AppThemeElement.shadow).cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: -10, height: 10)
        view.layer.shadowRadius = 15
    }
    
    func startTopAnimation () {
        UIView.animate(withDuration: 2.0, delay: 0.0, options: [.repeat, .autoreverse], animations: {
            self.messageLabel.alpha = 0.0
            self.secondMessageLabel.alpha = 1.0
        }) { (completion) in
            self.messageLabel.alpha = 1.0
            self.secondMessageLabel.alpha = 0.0
            self.startTopAnimation()
        }
    }
    
    func startPinAnimation () {
        UIView.animate(withDuration: 1.0, delay: 0.0, options: [.repeat, .autoreverse], animations: { 
            self.pinImageView.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            self.currentStatusLabel.alpha = 1.0
            }) { (completion) in
                self.pinImageView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                self.currentStatusLabel.alpha = 0.0
                self.startPinAnimation()
        }
    }
    
    func checkServiceNews () {
        if let currentService = self.currentService {
            PanaAPI.sharedAPI.getServiceInfo(Int(currentService.id), successBlock: { (service) in
                AppManager.shared.setService(service)
                print ("Service ID: \(service.id)")
                if service.status != "Solicitud enviada" && service.status != "Recibida por centro de control" {
                    self.topContainerView.isHidden = true
                }
                if service.finished || service.canceled {
                    AppManager.shared.serviceEnded()
                    _ = self.navigationController?.popViewController(animated: true)
                }
                self.currentStatusLabel.text = service.status
                self.bottomContainerView.layoutIfNeeded()
                }) { (error) in
                    print ("error updating status")
            }
        }
    }
    
    func endService () {
        if let currentService = self.currentService {
            AppManager.shared.showHUD(self.view, message: localizedStringForKey("loading"))
            PanaAPI.sharedAPI.endService(Int(currentService.id), successBlock: {
                AppManager.shared.hideHUD(self.view)
                AppManager.shared.serviceEnded()
                _ = self.navigationController?.popViewController(animated: true)
                }) { (error) in
                    AppManager.shared.hideHUD(self.view)
                    print ("Error changing service")
            }
        }
    }
    
    func cancelService () {
        presentModalView(.cancelList, completion: nil)
    }
    
    // MARK: - Button's Actions

    func callButtonTapped () {
        self.callOperationCenter ()
    }
    
    func cancelButtonTapped () {
        self.showCancelActionSheet ()
    }
    
    // MARK: - Alerts & Action Sheets
    
    func showCancelActionSheet () {
        let actionSheet = UIAlertController (title: localizedStringForKey("endService"), message: localizedStringForKey("endServiceMessage"), preferredStyle: UIAlertControllerStyle.actionSheet)
        let endAction = UIAlertAction (title: localizedStringForKey("markFinished"), style: UIAlertActionStyle.default) { (action) in
            self.endService()
        }
        let cancelAction = UIAlertAction (title: localizedStringForKey("cancelRequest"), style: UIAlertActionStyle.default) { (action) in
            self.cancelService()
        }
        let backAction = UIAlertAction (title: localizedStringForKey("back"), style: UIAlertActionStyle.cancel, handler: nil)
        actionSheet.addAction(endAction)
        actionSheet.addAction(cancelAction)
        actionSheet.addAction(backAction)
        self.present(actionSheet, animated: true, completion: nil)
    }
}

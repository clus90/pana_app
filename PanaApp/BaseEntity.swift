//
//  BaseEntity.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 8/16/16.
//  Copyright © 2016 Sappito Technologies, C.A. All rights reserved.
//

import UIKit
import Mantle

class BaseEntity: MTLModel, MTLJSONSerializing {
    
    class func jsonKeyPathsByPropertyKey() -> [AnyHashable: Any]! {
        // override
        return ["":""]
    }
}

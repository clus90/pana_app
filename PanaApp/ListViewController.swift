//
//  ListViewController.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 3/18/17.
//  Copyright © 2017 Sappito Technologies, C.A. All rights reserved.
//

import UIKit
import SnapKit

class ListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    var reasons: [CancelReason] = []
    var selectedReason: CancelReason?
    
    enum ListCellType: Int {
        case reason
        case separator
        case button
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        self.navigationItem.title = localizedStringForKey("cancel_reason")
        let cancelButton = UIBarButtonItem (image: UIImage(named: "Cancel_Icon"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.closeButtonTapped))
        self.navigationItem.leftBarButtonItem = cancelButton
        setupTableView()
        getReasons ()
    }
    
    override func needsLogoOnNavBar() -> Bool {
        return false
    }
    
    override func needsNavigationBarTransparent() -> Bool {
        return true
    }
    
    // MARK: - Setup
    
    func setupTableView() {
        let backgroundImageView = UIImageView (image: UIImage(named: "Background_Login"))
        backgroundImageView.contentMode = .scaleAspectFill
        tableView.backgroundView = backgroundImageView
        tableView.register(UINib(nibName: "LabelCell", bundle: nil), forCellReuseIdentifier: "LabelCell")
        tableView.register(UINib(nibName: "ButtonCell", bundle: nil), forCellReuseIdentifier: "ButtonCell")
        tableView.estimatedRowHeight = 50
        tableView.dataSource = self
        tableView.delegate = self
        tableView.allowsSelection = true
        tableView.allowsMultipleSelection = false
    }
    
    // MARK: - Datasource
    
    func cellList() -> [ListCellType] {
        var cells: [ListCellType] = []
        for _ in reasons {
            cells += [.reason]
        }
        cells += [.separator, .button]
        return cells
    }
    
    // MARK: - API Call
    
    func getReasons () {
        AppManager.shared.showHUD(self.view, message: localizedStringForKey("loading"))
        PanaAPI.sharedAPI.getCancelingReasons(successBlock: { (reasons) in
            AppManager.shared.hideHUD(self.view)
            self.reasons = reasons
            self.tableView.reloadData()
        }) { (error) in
            AppManager.shared.hideHUD(self.view)
            self.getReasons()
        }
    }
    
    // MARK: - Selectors
    
    func closeButtonTapped () {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func cancelService () {
        if let reason = selectedReason {
            if let id = AppManager.shared.currentService?.id as? Int {
                AppManager.shared.showHUD(self.view, message: localizedStringForKey("loading"))
                PanaAPI.sharedAPI.cancelService(id, reason: reason, successBlock: {
                    AppManager.shared.hideHUD(self.view)
                    self.closeButtonTapped()
                }, errorBlock: { (error) in
                    AppManager.shared.hideHUD(self.view)
                    self.showAlertSingleOption(localizedStringForKey("sorry"), message: localizedStringForKey("unable_to_cancel_service_error"))
                })
                
            }
        } else {
            showAlertSingleOption(localizedStringForKey("sorry"), message: localizedStringForKey("no_cancel_reason_error"))
        }
    }
    
    
    // MARK: - UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellList().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType:ListCellType = self.cellList()[indexPath.row]
        switch cellType {
        case .reason:
            let cell: LabelCell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath) as! LabelCell
            if indexPath.row == 0 {
                cell.shouldApplyTopCornerRadius = true
            } else if indexPath.row == reasons.count - 1 {
                cell.shouldApplyBottomCornerRadius = true
            }
            cell.label.text = reasons[indexPath.row].reason
            return cell
        case .separator:
            let cell = UITableViewCell ()
            cell.backgroundColor = .clear
            cell.selectionStyle = .none
            return cell
        case .button:
            let cell: ButtonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell", for: indexPath) as! ButtonCell
            cell.btn.addTarget(self, action: #selector(self.cancelService), for: .touchUpInside)
            cell.setupWith(title: localizedStringForKey("cancel_service"))
            cell.btn.titleLabel?.font = AppTheme.fontForThemeElement(.loginButton)
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    // MARK: - UITableViewDelegate methods
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell:LabelCell = tableView.cellForRow(at: indexPath) as? LabelCell {
            selectedReason = reasons[indexPath.row]
            cell.viewBackground.backgroundColor = AppTheme.backgroundColorForThemeElement(.appMainColor)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell:LabelCell = tableView.cellForRow(at: indexPath) as? LabelCell {
            selectedReason = nil
            cell.viewBackground.backgroundColor = .white
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellType:ListCellType = self.cellList()[indexPath.row]
        switch cellType {
        case .reason:
            return UITableViewAutomaticDimension
        case .separator:
            return 40
        case .button:
            return ButtonCell.preferredHeight()
        }
    }
    
    
}

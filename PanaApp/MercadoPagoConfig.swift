//
//  MercadoPagoConfig.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 4/13/17.
//  Copyright © 2017 Sappito Technologies, C.A. All rights reserved.
//

import UIKit
import Mantle

class MercadoPagoConfig: BaseEntity {
    
    var publicKey: String = ""
    var price: String = ""
    var months: String = ""
    
    override init() {
        super.init()
    }
    
    required init(dictionary dictionaryValue: [AnyHashable : Any]!) throws {
        try! super.init(dictionary: dictionaryValue)
    }
    
    override class func jsonKeyPathsByPropertyKey() -> [AnyHashable: Any]! {
        return ["publicKey":"public_key",
                "price":"price",
                "months":"months"]
    }
    
    func getPriceFormated () -> String {
        if let myInteger = Int(price) {
            let myNumber = NSNumber(value:myInteger)
            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            formatter.groupingSeparator = "."
            formatter.decimalSeparator = ","
            formatter.usesGroupingSeparator = true
            formatter.minimumFractionDigits = 2
            if let priceFormated = formatter.string(from: myNumber) {
                return "Bs. \(priceFormated)"
            }
        }
        return "Bs. \(price)"
    }
    
}

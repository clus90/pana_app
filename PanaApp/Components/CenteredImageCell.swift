//
//  CenteredImageCell.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 3/14/17.
//  Copyright © 2017 Sappito Technologies, C.A. All rights reserved.
//

import UIKit

class CenteredImageCell: UITableViewCell {
    
    @IBOutlet weak var imgView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

//
//  TextFieldTableCell.swift
//  InConcert
//
//  Created by Carlos Luis Urbina on 2/16/17.
//  Copyright © 2017 SappitoTech. All rights reserved.
//

import UIKit

class TextFieldCell: UITableViewCell {
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var underlineView: UIView!
    @IBOutlet weak var errorView: UIImageView!
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var imgView: UIImageView!
    
    var shouldApplyTopCornerRadius:Bool = false
    var shouldApplyBottomCornerRadius:Bool = false
    var shouldHideUnderLineView:Bool = false

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        textField.textColor = AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.textField)
        textField.font = AppTheme.fontForThemeElement(.textField)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        underlineView.isHidden = shouldHideUnderLineView    
        
        if (!shouldApplyTopCornerRadius && !shouldApplyBottomCornerRadius) {
            return
        }
        
        let maskPath:UIBezierPath = UIBezierPath(roundedRect: self.viewBackground.bounds, byRoundingCorners: (shouldApplyTopCornerRadius) ? [.topLeft, .topRight] : [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: 5, height: 5))
        
        let maskLayer:CAShapeLayer = CAShapeLayer()
        maskLayer.frame = self.viewBackground.bounds
        maskLayer.path = maskPath.cgPath
        self.viewBackground.layer.mask = maskLayer
        
    }
    
    func applyCornerRadiusToAllCorners () {
        self.viewBackground.layer.cornerRadius = 5.0
    }
    
    func hasText() -> Bool {
        return (textField.hasText)
    }
    
    func isEmailTextValid() -> Bool {
        return isValidEmail(textField.text)
    }
    
    func isNumberTextValid () -> Bool {
        let badCharacters = CharacterSet.decimalDigits.inverted
        return textField.text?.rangeOfCharacter(from: badCharacters) == nil
    }
    
    func isNoSpacesValid () -> Bool {
        let badCharacters = CharacterSet.whitespaces
        return textField.text?.rangeOfCharacter(from: badCharacters) == nil
    }
    
    func needsToDisplayError(_ display:Bool) {
        self.errorView.isHidden = !display
    }
    
    // MARK: - Class functions
    class func preferredHeight() -> CGFloat {
        return 50
    }
    
}

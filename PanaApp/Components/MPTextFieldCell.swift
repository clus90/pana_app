//
//  MPTextFieldCell.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 7/12/17.
//  Copyright © 2017 Sappito Technologies, C.A. All rights reserved.
//

import UIKit
import SnapKit

class MPTxtField: UITextField {
    override public func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(copy(_:)) || action == #selector(paste(_:)) || action == #selector(cut(_:)) || action == #selector(select(_:)) || action == #selector(selectAll(_:)) {
            return false
        }
        
        return true
    }
}

class MPTextFieldCell: UITableViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: MPTxtField!
    @IBOutlet weak var errorView: UIImageView!
    
    var inputType: InputType = .cardNumber
    
    enum InputType: Int {
        case paymentType
        case cardNumber
        case securityCode
        case expirationDate
        case name
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        textField.textColor = AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.textField)
        textField.font = AppTheme.fontForThemeElement(.textField)
        textField.delegate = self
        textField.clearButtonMode = .whileEditing
        self.selectionStyle = .none
    }
    
    func setupFor (type: InputType) {
        self.inputType = type
        switch type {
        case .paymentType:
            label.text = localizedStringForKey("payment_type")
            break
        case .cardNumber:
            label.text = localizedStringForKey("card_number")
            textField.keyboardType = .numberPad
            break
        case .securityCode:
            label.text = localizedStringForKey("security_code")
            textField.placeholder = "XXX"
            textField.keyboardType = .numberPad
            break
        case .expirationDate:
            label.text = localizedStringForKey("expiration_date")
            textField.placeholder = "MM / AA"
            textField.keyboardType = .numberPad
            break
        case .name:
            label.text = localizedStringForKey("name_on_card")
            textField.keyboardType = .default
            textField.autocapitalizationType = .words
            break
        }
    }
    
    func needsToDisplayError(_ display:Bool) {
        self.errorView.isHidden = !display
    }
    
    func getValue () -> String? {
        switch inputType {
        case .paymentType:
//            if let text = textField.text, textField.hasText, text != localizedStringForKey("select") {
//                errorView.isHidden = true
//                return text
//            }
            break
        case .cardNumber:
            if let text = textField.text {
                errorView.isHidden = true
                return text.replacingOccurrences(of: " ", with:"")
            }
            break
        case .securityCode:
            if let text = textField.text {
                errorView.isHidden = true
                return text
            }
            break
        case .expirationDate:
            if let text = textField.text {
                errorView.isHidden = true
                return text
            }
            break
        case .name:
            if let text = textField.text {
                errorView.isHidden = true
                return text
            }
            break
        }
        errorView.isHidden = false
        return nil
    }
    
    func getExpirationMonth () -> Int? {
        if inputType != .expirationDate {
            errorView.isHidden = false
            return nil
        }
        if let text = textField.text, textField.hasText {
            var monthStr : String = text.characters.split {$0 == "/"}.map(String.init)[0] as String
            monthStr = monthStr.trimmingCharacters(in: CharacterSet.whitespaces) as String
            errorView.isHidden = true
            return monthStr.numberValue as? Int
        }
        errorView.isHidden = false
        return nil
    }
    
    func getExpirationYear () -> Int? {
        if inputType != .expirationDate {
            errorView.isHidden = false
            return nil
        }
        if let text = textField.text, textField.hasText {
            var yearStr : String = text.characters.split {$0 == "/"}.map(String.init)[1] as String
            yearStr = yearStr.trimmingCharacters(in: CharacterSet.whitespaces) as String
            errorView.isHidden = true
            return yearStr.numberValue as? Int
        }
        errorView.isHidden = false
        return nil
    }
    
    // MARK: - Class functions
    class func preferredHeight() -> CGFloat {
        return 80
    }
    
    // MARK: - TextFieldDelegate
    
    func textField(_ textField: UITextField,shouldChangeCharactersIn range: NSRange,    replacementString string: String) -> Bool {
        switch inputType {
        case .cardNumber:
            if textField.text != nil {
                let maxLength = 16
                let spaces = 3
                var txtAfterUpdate : NSString = textField.text! as NSString
                txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
                if txtAfterUpdate.length <= maxLength + spaces {
                    if txtAfterUpdate.length > 4 {
                        let cardNumber : NSString = txtAfterUpdate.replacingOccurrences(of: " ", with:"") as NSString
                        // 4 4 4 4
                        let mutableString : NSMutableString = NSMutableString(capacity: maxLength + spaces)
                        for i in 0...(cardNumber.length-1) {
                            if i > 0 && i%4 == 0 {
                                mutableString.appendFormat(" %C", cardNumber.character(at: i))
                            } else {
                                mutableString.appendFormat("%C", cardNumber.character(at: i))
                            }
                        }
                        self.textField.text = mutableString as String
                        return false
                    }
                    return true
                }
            }
        case .securityCode:
            var txtAfterUpdate : NSString? = self.textField.text as NSString?
            if txtAfterUpdate != nil {
                txtAfterUpdate = txtAfterUpdate!.replacingCharacters(in: range, with: string) as NSString?
                if txtAfterUpdate!.length <= 3 {
                    return true
                }
            }

        case .expirationDate:
            if textField.text != nil {
                var txtAfterUpdate : NSString = textField.text! as NSString
                txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
                var str : String = ""
                if txtAfterUpdate.length <= 7 {
                    var date : NSString = txtAfterUpdate.replacingOccurrences(of: " ", with:"") as NSString
                    date = date.replacingOccurrences(of: "/", with:"") as NSString
                    if date.length >= 1 && date.length <= 4 {
                        for i in 0...(date.length-1) {
                            if i == 2 {
                                str = str + " / "
                            }
                            str = str + String(format: "%C", date.character(at: i))
                        }
                    }
                    self.textField.text = str
                }
            }
        case .name, .paymentType:
            return true
        }
        
        
        return false
    }
}

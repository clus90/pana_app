//
//  MPUserIdCell.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 7/12/17.
//  Copyright © 2017 Sappito Technologies, C.A. All rights reserved.
//

import UIKit
import MercadoPagoSDK

class MPUserIdCell: UITableViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var documentLabel: UILabel!
    @IBOutlet weak var typeTextField: UITextField!
    @IBOutlet weak var documentTextField: MPTxtField!
    @IBOutlet weak var errorView: UIImageView!
    
    var identificationTypes : [IdentificationType] = [IdentificationType]()
    var identificationType : IdentificationType?
    var idTypePicker: UIPickerView = UIPickerView ()
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        typeLabel.text = localizedStringForKey("type")
        documentLabel.text = localizedStringForKey("document")
        documentTextField.textColor = AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.textField)
        documentTextField.font = AppTheme.fontForThemeElement(.textField)
        documentTextField.delegate = self
        documentTextField.clearButtonMode = .whileEditing
        documentTextField.keyboardType = .numberPad
        typeTextField.textColor = AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.textField)
        typeTextField.font = AppTheme.fontForThemeElement(.textField)
        typeTextField.inputView = idTypePicker
        idTypePicker.delegate = self
        idTypePicker.dataSource = self
        self.selectionStyle = .none
    }
    
    // MARK: - Class functions
    class func preferredHeight() -> CGFloat {
        return 80
    }

    func typeSelectedAt(_ row: Int) {
        identificationType = identificationTypes[row]
        typeTextField.text = identificationTypes[row].name
    }
    
    func needsToDisplayError(_ display:Bool) {
        self.errorView.isHidden = !display
    }
    
    func getIdType () -> String? {
        if let idType = typeTextField.text {
            return idType
        }
        return nil
    }
    
    func getIdNumber () -> String? {
        if let document = documentTextField.text {
            return document
        }
        return nil
    }
    
    // MARK: - TextField Delegate
    
    func textField(_ textField: UITextField,shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.documentTextField {
            if textField.text != nil {
                var txtAfterUpdate: NSString = textField.text! as NSString
                txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
                if let maxLength = self.identificationType?.maxLength, txtAfterUpdate.length <= maxLength {
                    return true
                }
            }
            return false
        }
        return false
    }
    
}

extension MPUserIdCell: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return identificationTypes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return identificationTypes[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        typeSelectedAt(row)
    }
    
}

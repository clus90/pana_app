//
//  MPSecurityCodeCell.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 7/17/17.
//  Copyright © 2017 Sappito Technologies, C.A. All rights reserved.
//

import UIKit
import MercadoPagoSDK

class MPSecurityCodeCell: UITableViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var securityCodeTextField: MPTxtField!
    @IBOutlet weak var cardImageView: UIImageView!
    @IBOutlet weak var errorView: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        titleLabel.text = localizedStringForKey("security_code")
        securityCodeTextField.textColor = AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.textField)
        securityCodeTextField.font = AppTheme.fontForThemeElement(.textField)
        securityCodeTextField.delegate = self
        securityCodeTextField.keyboardType = .numberPad

    }
    
    func needsToDisplayError(_ display:Bool) {
        self.errorView.isHidden = !display
    }

    func fillWithPaymentMethod(_ pm : PaymentMethod) {
        self.cardImageView.image = MercadoPago.getImage("imgTc_" + pm._id)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var txtAfterUpdate : NSString? = textField.text as NSString?
        if txtAfterUpdate != nil {
            txtAfterUpdate = txtAfterUpdate!.replacingCharacters(in: range, with: string) as NSString?
            if txtAfterUpdate!.length <= 3 {
                return true
            }
        }
        return false
    }
    
    
    
    // MARK: - Class functions
    class func preferredHeight() -> CGFloat {
        return 80
    }
    
}

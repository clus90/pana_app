//
//  LabelCell.swift
//  InConcert
//
//  Created by Carlos Luis Urbina on 3/6/17.
//  Copyright © 2017 SappitoTech. All rights reserved.
//

import UIKit
import SnapKit

class LabelCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var underlineView: UIView!
    
    var shouldApplyTopCornerRadius:Bool = false
    var shouldApplyBottomCornerRadius:Bool = false
    var shouldHideUnderLineView:Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        underlineView.isHidden = shouldHideUnderLineView
        
        if (!shouldApplyTopCornerRadius && !shouldApplyBottomCornerRadius) {
            return
        }
        
        let maskPath:UIBezierPath = UIBezierPath(roundedRect: self.viewBackground.bounds, byRoundingCorners: (shouldApplyTopCornerRadius) ? [.topLeft, .topRight] : [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: 5, height: 5))
        
        let maskLayer:CAShapeLayer = CAShapeLayer()
        maskLayer.frame = self.viewBackground.bounds
        maskLayer.path = maskPath.cgPath
        self.viewBackground.layer.mask = maskLayer
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setForClearMode () {
        shouldHideUnderLineView = true
        viewBackground.isHidden =  true
    }
    
    func setTextForTitle (text: String) {
        label.text = text
        label.font = UIFont.systemFont(ofSize: 20.0, weight: UIFontWeightSemibold)
    }
    
    func setTextForSubtitle (text: String) {
        label.text = text
        label.font = UIFont.systemFont(ofSize: 13.0)
    }
    
}

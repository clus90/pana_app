//
//  DoubleTextFieldCell.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 3/14/17.
//  Copyright © 2017 Sappito Technologies, C.A. All rights reserved.
//

import UIKit

class DoubleTextFieldCell: UITableViewCell {
    
    @IBOutlet weak var leftTextField: UITextField!
    @IBOutlet weak var rightTextField: UITextField!
    @IBOutlet weak var underlineView: UIView!
    @IBOutlet weak var errorView: UIImageView!
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var imgView: UIImageView!
    
    var shouldApplyTopCornerRadius:Bool = false
    var shouldApplyBottomCornerRadius:Bool = false
    var shouldHideUnderLineView:Bool = false

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        leftTextField.textColor = AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.textField)
        leftTextField.font = AppTheme.fontForThemeElement(.textField)
        rightTextField.textColor = AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.textField)
        rightTextField.font = AppTheme.fontForThemeElement(.textField)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        underlineView.isHidden = shouldHideUnderLineView
        
        if (!shouldApplyTopCornerRadius && !shouldApplyBottomCornerRadius) {
            return
        }
        
        let maskPath:UIBezierPath = UIBezierPath(roundedRect: self.viewBackground.bounds, byRoundingCorners: (shouldApplyTopCornerRadius) ? [.topLeft, .topRight] : [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: 5, height: 5))
        
        let maskLayer:CAShapeLayer = CAShapeLayer()
        maskLayer.frame = self.viewBackground.bounds
        maskLayer.path = maskPath.cgPath
        self.viewBackground.layer.mask = maskLayer
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func hasText() -> Bool {
        return (rightTextField.hasText)
    }
    
    func isNumberTextValid () -> Bool {
        let badCharacters = CharacterSet.decimalDigits.inverted
        return rightTextField.text?.rangeOfCharacter(from: badCharacters) == nil
    }
    
    func isNoSpacesValid () -> Bool {
        let badCharacters = CharacterSet.whitespaces
        return rightTextField.text?.rangeOfCharacter(from: badCharacters) == nil
    }
    
    func needsToDisplayError(_ display:Bool) {
        self.errorView.isHidden = !display
    }

    
}

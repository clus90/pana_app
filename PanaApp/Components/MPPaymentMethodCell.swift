//
//  MPPaymentMethodCell.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 7/11/17.
//  Copyright © 2017 Sappito Technologies, C.A. All rights reserved.
//

import UIKit
import MercadoPagoSDK

class MPPaymentMethodCell: UITableViewCell {
    
    @IBOutlet weak var paymentMethodImage: UIImageView!
    @IBOutlet weak var paymentMethodLabel: UILabel!
    @IBOutlet weak var underlineView: UIView!
    @IBOutlet weak var viewBackground: UIView!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setLabel(_ label : String) {
        self.paymentMethodLabel.text = label
    }
    
    func setImageWithName(_ imageName : String) {
        self.paymentMethodImage.image = MercadoPago.getImage(imageName)
    }
    
    func fillWithCard(_ card : Card?) {
        if card == nil || card!.paymentMethod == nil {
            return
        } else {
            self.paymentMethodLabel.text = localizedStringForKey ("card_finished_with") + " " + card!.lastFourDigits!
            self.paymentMethodImage.image = MercadoPago.getImage(card!.paymentMethod!._id)
        }
    }
    
}

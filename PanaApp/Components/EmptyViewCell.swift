//
//  EmptyViewCell.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 7/11/17.
//  Copyright © 2017 Sappito Technologies, C.A. All rights reserved.
//

import UIKit
import SnapKit

class EmptyViewCell: UITableViewCell {
    
    @IBOutlet weak var viewBackground: UIView!
    
    
    var shouldApplyTopCornerRadius:Bool = false
    var shouldApplyBottomCornerRadius:Bool = false

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        if (!shouldApplyTopCornerRadius && !shouldApplyBottomCornerRadius) {
            return
        }
        
        let maskPath:UIBezierPath = UIBezierPath(roundedRect: self.viewBackground.bounds, byRoundingCorners: (shouldApplyTopCornerRadius) ? [.topLeft, .topRight] : [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: 5, height: 5))
        
        let maskLayer:CAShapeLayer = CAShapeLayer()
        maskLayer.frame = self.viewBackground.bounds
        maskLayer.path = maskPath.cgPath
        self.viewBackground.layer.mask = maskLayer
        
    }
    
    func setWith (view: UIView) {
        viewBackground.addSubview(view)
        view.snp.makeConstraints { (make) in
            make.edges.equalTo(viewBackground)
        }
    }
    
}

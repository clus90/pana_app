//
//  ButtonCell.swift
//  InConcert
//
//  Created by Carlos Luis Urbina on 2/16/17.
//  Copyright © 2017 SappitoTech. All rights reserved.
//

import UIKit
import SnapKit

class ButtonCell: UITableViewCell {
    
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var btnHeightConstraint: NSLayoutConstraint!
    
    enum ButtonType: Int {
        case login
        case signUp
        case facebook
        case forgotPassword
        case buy
        case media
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }
    
    func setupWith (title: String) {
        btn.setTitle(title, for: .normal)
        btn.setTitleColor(AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.loginButton), for: UIControlState.normal)
        btn.backgroundColor = AppTheme.backgroundColorForThemeElement(AppTheme.AppThemeElement.loginButton)
        btn.layer.cornerRadius = 5
    }
    
    func setupUnderlinedWith (title: String) {
        let rUnderlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue, NSForegroundColorAttributeName : AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.loginLabel)] as [String : Any]
        let rUnderlineAttributedString = NSAttributedString(string: title, attributes: rUnderlineAttribute)
        btn.setAttributedTitle(rUnderlineAttributedString, for: .normal)
    }

    
    // MARK: - Class functions
    
    class func preferredHeight() -> CGFloat {
        return 60
    }

    
}

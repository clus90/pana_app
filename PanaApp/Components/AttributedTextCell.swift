//
//  AttributedTextCell.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 3/14/17.
//  Copyright © 2017 Sappito Technologies, C.A. All rights reserved.
//

import UIKit
import AKAttributeKit

class AttributedTextCell: UITableViewCell {

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var btn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.textView.isEditable = false
        self.textView.isScrollEnabled = false
        self.textView.delegate = self
        self.btn.layer.cornerRadius = 3.0
    }
    
    func setAttributedText (_ text: String) {
        textView.attributedText = AKAttributeKit.parseString(text)
        self.textView.font = AppTheme.fontForThemeElement(.loginLabel)
        self.textView.textColor = AppTheme.textColorForThemeElement(.loginLabel)
        self.textView.textAlignment = NSTextAlignment.center
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension AttributedTextCell: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        return true
    }
}

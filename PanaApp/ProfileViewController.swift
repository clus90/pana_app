//
//  ProfileViewController.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 3/16/17.
//  Copyright © 2017 Sappito Technologies, C.A. All rights reserved.
//

import UIKit
import CountryPicker

class ProfileViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    enum ProfileCellType: Int {
        case smallSeparator
        case firstName
        case lastName
        case email
        case phone
        case id
        case paymentExpirationDate
        case editButton
        case changePassword
    }
    
    enum ProfileActiveProtocol: Int {
        case regular
        case edit
    }
    
    // Set default protocol to SignUp
    var activeProtocol:ProfileActiveProtocol = .regular
    
    var user: User = User ()
    var countryCodePicker = CountryPicker ()
    let idTypePicker = UIPickerView ()
    
    var firstNameTextFieldCell:TextFieldCell?
    var lastNameTextFieldCell:TextFieldCell?
    var emailTextFieldCell:TextFieldCell?
    var phoneTextFieldCell: DoubleTextFieldCell?
    var idTextFieldCell: DoubleTextFieldCell?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        let cancelButton = UIBarButtonItem (image: UIImage(named: "Cancel_Icon"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.closeButtonTapped))
        self.navigationItem.leftBarButtonItem = cancelButton
        loadTableView()
        setupCountryPicker()
        setupPickerView()
        checkForUser ()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func needsNavigationBarTransparent() -> Bool {
        return true
    }
    
    func loadTableView() {
        let backgroundImageView = UIImageView (image: UIImage(named: "Background_Login"))
        backgroundImageView.contentMode = .scaleAspectFill
        tableView.backgroundView = backgroundImageView
        tableView.register(UINib(nibName: "TextFieldCell", bundle: nil), forCellReuseIdentifier: "TextFieldCell")
        tableView.register(UINib(nibName: "DoubleTextFieldCell", bundle: nil), forCellReuseIdentifier: "DoubleTextFieldCell")
        tableView.register(UINib(nibName: "ButtonCell", bundle: nil), forCellReuseIdentifier: "ButtonCell")
        tableView.register(UINib(nibName: "LabelCell", bundle: nil), forCellReuseIdentifier: "LabelCell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.allowsSelection = false
    }
    
    func setupCountryPicker () {
        countryCodePicker.countryPickerDelegate = self
        if user.countryCode != "" {
            countryCodePicker.setCountryByPhoneCode(user.countryCode)
            return
        }
        countryCodePicker.setCountry("VE")
    }
    
    func setupPickerView () {
        idTypePicker.backgroundColor = UIColor.lightGray
        idTypePicker.delegate = self
        idTypePicker.dataSource = self
    }
    
    func checkForUser () {
        if let user = AppManager.shared.user {
            self.user = user
            self.tableView.reloadData()
        } else {
            AppManager.shared.getUser(completion: { 
                self.checkForUser()
            })
        }
    }
    
    func updateUser (user: User) {
        AppManager.shared.showHUD(self.view, message: localizedStringForKey("loading"))
        PanaAPI.sharedAPI.updateProfile(user, successBlock: {
            AppManager.shared.hideHUD(self.view)
            self.user = user
            AppManager.shared.user = user
            self.activeProtocol = .regular
            self.tableView.reloadData()
        }) { (error) in
            AppManager.shared.hideHUD(self.view)
            self.showAlertSingleOption(localizedStringForKey("sorry"), message: localizedStringForKey("update_user_error"))
        }
    }
    
    func profileCellList() -> [ProfileCellType] {
        switch activeProtocol {
        case .regular:
            return [.smallSeparator, .firstName, .lastName, .email, .phone, .id, .smallSeparator , .editButton, .changePassword, .smallSeparator, .paymentExpirationDate]
        case .edit:
            return [.smallSeparator, .firstName, .lastName, .phone, .id, .smallSeparator , .editButton]
        }
    }
    
    // MARK: - Actions and Selectors
    
    func editButtonTapped () {
        switch activeProtocol {
        case .regular:
            activeProtocol = .edit
            tableView.reloadData()
            break
        case .edit:
            if !validateForEditing(){
                return
            }
            if let firstName = firstNameTextFieldCell?.textField.text, let lastName = lastNameTextFieldCell?.textField.text, let email = emailTextFieldCell?.textField.text, let countryCode = phoneTextFieldCell?.leftTextField.text, let phone = phoneTextFieldCell?.rightTextField.text, let idType = idTextFieldCell?.leftTextField.text, let id = idTextFieldCell?.rightTextField.text {
                let tempUser = self.user
                tempUser.firstName = firstName
                tempUser.lastName = lastName
                tempUser.email = email
                tempUser.document = id
                tempUser.documentType = idType
                tempUser.phone = phone
                tempUser.countryCode = countryCode
                updateUser (user: tempUser)
                
            }
            
            break
        }
    }
    
    func changePasswordButtonTapped () {
        self.pushToView(.changePassword, completion: nil)
    }
    
    func closeButtonTapped () {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Validation Methods
    
    func validateForEditing () -> Bool {
        var isValidated: Bool = true
        isValidated = isValidated && isEmailTextFieldValid()
        
        if (!firstNameTextFieldCell!.hasText()) {
            isValidated = false
            firstNameTextFieldCell!.needsToDisplayError(true)
        } else {
            firstNameTextFieldCell!.needsToDisplayError(false)
        }
        if (!lastNameTextFieldCell!.hasText()) {
            isValidated = false
            lastNameTextFieldCell!.needsToDisplayError(true)
        } else {
            lastNameTextFieldCell!.needsToDisplayError(false)
        }
        if (!phoneTextFieldCell!.hasText()) {
            isValidated = false
            phoneTextFieldCell!.needsToDisplayError(true)
        } else {
            if let length = phoneTextFieldCell?.rightTextField.text?.length, length != 10 {
                isValidated = false
                phoneTextFieldCell!.needsToDisplayError(true)
            } else {
                phoneTextFieldCell!.needsToDisplayError(false)
            }
        }
        if (!idTextFieldCell!.hasText()) {
            isValidated = false
            idTextFieldCell!.needsToDisplayError(true)
        } else {
            if let length = idTextFieldCell?.rightTextField.text?.length, length > 8 {
                isValidated = false
                idTextFieldCell!.needsToDisplayError(true)
            } else {
                idTextFieldCell!.needsToDisplayError(false)
            }
        }
    
        return isValidated
    }
    
    func isEmailTextFieldValid() -> Bool {
        
        var isValidated:Bool = true
        
        if (!emailTextFieldCell!.hasText() || !emailTextFieldCell!.isEmailTextValid()) {
            isValidated = false
            emailTextFieldCell!.needsToDisplayError(true)
        } else {
            emailTextFieldCell!.needsToDisplayError(false)
        }
        
        return isValidated
    }
    
    
    func facebookButtonTapped(_ sender:UIButton) {
        
    }
    
    // MARK: - UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profileCellList().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType:ProfileCellType = profileCellList()[indexPath.row]
        
        switch cellType {
        case .firstName:
            let cell: TextFieldCell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.placeholder = localizedStringForKey("first_name")
            cell.textField.autocapitalizationType = .words
            cell.textField.returnKeyType = UIReturnKeyType.done
            cell.textField.text = user.firstName
            cell.textField.isEnabled = activeProtocol == .edit
            cell.shouldApplyTopCornerRadius = true
            cell.imgView.image = UIImage (named: "Pencil_Image")?.withRenderingMode(.alwaysTemplate)
            cell.imgView.tintColor = activeProtocol == .regular ? AppTheme.backgroundColorForThemeElement(.separatorLine) : AppTheme.backgroundColorForThemeElement(.appMainColor)
            firstNameTextFieldCell = cell
            return cell
        case .lastName:
            let cell: TextFieldCell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.placeholder = localizedStringForKey("last_name")
            cell.textField.autocapitalizationType = .words
            cell.textField.returnKeyType = UIReturnKeyType.done
            cell.textField.text = user.lastName
            cell.textField.isEnabled = activeProtocol == .edit
            cell.imgView.image = UIImage (named: "Pencil_Image")?.withRenderingMode(.alwaysTemplate)
            cell.imgView.tintColor = activeProtocol == .regular ? AppTheme.backgroundColorForThemeElement(.separatorLine) : AppTheme.backgroundColorForThemeElement(.appMainColor)
            lastNameTextFieldCell = cell
            return cell
        case .email:
            let cell: TextFieldCell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.placeholder = localizedStringForKey("email")
            cell.textField.keyboardType = .emailAddress
            cell.textField.autocapitalizationType = .none
            cell.textField.returnKeyType = UIReturnKeyType.done
            cell.textField.text = user.email
            cell.textField.isEnabled = false
            cell.imgView.image = UIImage (named: "Email_Image")
            emailTextFieldCell = cell
            return cell
        case .phone:
            let cell: DoubleTextFieldCell = tableView.dequeueReusableCell(withIdentifier: "DoubleTextFieldCell", for: indexPath) as! DoubleTextFieldCell
            cell.leftTextField.text = "+58"
            cell.rightTextField.placeholder = localizedStringForKey("phone_number")
            cell.rightTextField.keyboardType = .phonePad
            cell.leftTextField.inputView = countryCodePicker
            cell.leftTextField.text = user.countryCode
            cell.rightTextField.text = user.phone
            cell.leftTextField.isEnabled = activeProtocol == .edit
            cell.rightTextField.isEnabled = activeProtocol == .edit
            cell.imgView.image = UIImage (named: "Phone_Icon")?.withRenderingMode(.alwaysTemplate)
            cell.imgView.tintColor = activeProtocol == .regular ? AppTheme.backgroundColorForThemeElement(.separatorLine) : AppTheme.backgroundColorForThemeElement(.appMainColor)
            phoneTextFieldCell = cell
            return cell
        case .id:
            let cell: DoubleTextFieldCell = tableView.dequeueReusableCell(withIdentifier: "DoubleTextFieldCell", for: indexPath) as! DoubleTextFieldCell
            cell.rightTextField.placeholder = localizedStringForKey("id")
            cell.rightTextField.keyboardType = .numberPad
            cell.rightTextField.returnKeyType = UIReturnKeyType.done
            cell.leftTextField.text = user.documentType
            cell.rightTextField.text = user.document
            cell.leftTextField.inputView = idTypePicker
            cell.leftTextField.isEnabled = activeProtocol == .edit
            cell.rightTextField.isEnabled = activeProtocol == .edit
            cell.imgView.image = UIImage (named: "Pencil_Image")?.withRenderingMode(.alwaysTemplate)
            cell.imgView.tintColor = activeProtocol == .regular ? AppTheme.backgroundColorForThemeElement(.separatorLine) : AppTheme.backgroundColorForThemeElement(.appMainColor)
            cell.shouldHideUnderLineView = true
            cell.shouldApplyBottomCornerRadius = true
            idTextFieldCell = cell
            return cell
        case .editButton:
            let cell: ButtonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell", for: indexPath) as! ButtonCell
            cell.btn.addTarget(self, action: #selector(self.editButtonTapped), for: .touchUpInside)
            let title = activeProtocol == .regular ? localizedStringForKey("edit") : localizedStringForKey("save")
            cell.setupWith(title: title)
            cell.btn.titleLabel?.font = AppTheme.fontForThemeElement(.loginButton)
            return cell
        case .changePassword:
            let cell: ButtonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell", for: indexPath) as! ButtonCell
            cell.btn.addTarget(self, action: #selector(self.changePasswordButtonTapped), for: .touchUpInside)
            cell.setupUnderlinedWith(title: localizedStringForKey("change_password"))
            return cell
        case .paymentExpirationDate:
            let cell: LabelCell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath) as! LabelCell
            cell.label.text = "\(localizedStringForKey("payment_expires_in")): \(user.getReadableDate())"
            cell.label.textColor = AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.loginLabel)
            cell.label.textAlignment = .center
            cell.label.font = AppTheme.fontForThemeElement(.loginButton)
            cell.setForClearMode()
            return cell
        case .smallSeparator:
            let cell = UITableViewCell ()
            cell.backgroundColor = .clear
            return cell
        }
    }
    
    // MARK: - UITableViewDelegate methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellType:ProfileCellType = self.profileCellList()[(indexPath as NSIndexPath).row]
        
        switch cellType {
        case .editButton:
            return ButtonCell.preferredHeight()
        case .smallSeparator:
            return 30
        case .email, .firstName, .lastName, .phone, .id:
            return TextFieldCell.preferredHeight()
        case .changePassword, .paymentExpirationDate:
            return 50
        }
    }
    
}

// MARK: - CountryPicker Delegate

extension ProfileViewController: CountryPickerDelegate {
    
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        if let textField = phoneTextFieldCell?.leftTextField {
            textField.text = phoneCode
        }
    }
    
}

// MARK: - PickerView Delegate and DataSource

extension ProfileViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return kIdTypes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerView.reloadAllComponents()
        if let textField = idTextFieldCell?.leftTextField {
            textField.text = kIdTypes[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let color = (row == pickerView.selectedRow(inComponent: component)) ? UIColor.green : UIColor.black
        return NSAttributedString(string: kIdTypes[row], attributes: [NSForegroundColorAttributeName: color])
    }
}

//
//  MenuTableViewController.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 7/10/16.
//  Copyright © 2016 Sappito Technologies, C.A. All rights reserved.
//

import UIKit
import SnapKit

class MenuViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    enum MenuCellType: Int {
        case separator
        case profile
        case makePayment
        case terms
        case about
        case logOut
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupTableView()
    }
    
    func setupTableView() {
        tableView.register(UINib(nibName: "LabelCell", bundle: nil), forCellReuseIdentifier: "LabelCell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.allowsSelection = true
        tableView.allowsMultipleSelection = false
        tableView.separatorStyle = .singleLine
        tableView.separatorColor = .darkGray
        tableView.tableFooterView = UIView()
        tableView.reloadData()
    }
    
    func dataSource() -> [MenuCellType] {
        if let user  = AppManager.shared.user, !user.mercadoPago  {
            return [.separator, .profile, .makePayment, .terms, .about, .logOut]
        }
        return [.separator, .profile, .terms, .about, .logOut]
    }
    
    // MARK: - UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: LabelCell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath) as! LabelCell
        cell.label.textColor = AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.loginLabel)
        cell.setForClearMode()
        let cellType: MenuCellType = dataSource()[indexPath.row]
        switch cellType {
        case .separator:
            cell.label.text = ""
            break
        case .profile:
            cell.label.text = localizedStringForKey("profile")
            break
        case .makePayment:
            cell.label.text = localizedStringForKey("make_payment")
        case .terms:
            cell.label.text = localizedStringForKey("terms")
            break
        case .about:
            cell.label.text = localizedStringForKey("about")
        case .logOut:
            cell.label.text = localizedStringForKey("log_out")
            break
        }
        return cell
    }
    
    // MARK: - UITableViewDelegate methods
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellType: MenuCellType = dataSource()[indexPath.row]
        switch cellType {
        case .separator:
            break
        case .profile:
            AppManager.shared.rootViewController.closeDrawer(animated: true, completion: { (completed) in
                let nav = ViewFactory.viewControllerForAppView(ViewFactory.AppView.profile) as! UINavigationController
                AppManager.shared.rootViewController.present(nav, animated: true, completion: nil)
            })
            break
        case .makePayment:
            AppManager.shared.rootViewController.closeDrawer(animated: true, completion: { (completed) in
                self.getMPCards()
            })
            break
        case .terms:
            AppManager.shared.rootViewController.closeDrawer(animated: true, completion: { (completed) in
                let nav = ViewFactory.viewControllerForAppView(ViewFactory.AppView.webView) as! UINavigationController
                AppManager.shared.rootViewController.present(nav, animated: true, completion: {
                    let vc = nav.topViewController! as! WebViewController
                    vc.webView.loadRequest(URLRequest (url: URL(string: kTermsURL)!))
                })
                
            })
            break
        case .about:
            AppManager.shared.rootViewController.closeDrawer(animated: true, completion: { (completed) in
                let nav = ViewFactory.viewControllerForAppView(ViewFactory.AppView.webView) as! UINavigationController
                AppManager.shared.rootViewController.present(nav, animated: true, completion: {
                    let vc = nav.topViewController! as! WebViewController
                    vc.webView.loadRequest(URLRequest (url: URL(string: kAboutURL)!))
                })
                
            })
            break
        case .logOut:
            AppManager.shared.rootViewController.closeDrawer(animated: true, completion: { (completed) in
                AppManager.shared.logOut()
            })
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

    func getMPCards () {
        AppManager.shared.showHUD(self.view, message: localizedStringForKey("loading"))
        PanaAPI.sharedAPI.getUserCards(successBlock: { (cards) in
            AppManager.shared.hideHUD(self.view)
            print ("count: \(cards.count)")
            // TODO:
            if cards.count > 1 {
                let nav = ViewFactory.viewControllerForAppView(ViewFactory.AppView.mp_Cards) as! UINavigationController
                AppManager.shared.rootViewController.present(nav, animated: true, completion: {
                    if let vc = nav.topViewController as? MPMainViewController {
                        vc.loadItemsFrom(cards: cards)
                    }
                })
            } else {
                let nav = ViewFactory.viewControllerForAppView(ViewFactory.AppView.mp_NewCardController) as! UINavigationController
                AppManager.shared.rootViewController.present(nav, animated: true, completion: nil)
            }
        }) { (error) in
            AppManager.shared.hideHUD(self.view)
            self.showAlertSingleOption(localizedStringForKey("sorry"), message: localizedStringForKey("unable_to_get_cards_error"))
        }
    }
    
    
}

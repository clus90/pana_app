//
//  PasswordChangeViewController.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 4/13/17.
//  Copyright © 2017 Sappito Technologies, C.A. All rights reserved.
//

import UIKit

class PasswordChangeViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    enum PasswordCellType: Int {
        case topSeparator
        case smallSeparator
        case email
        case oldPassword
        case password
        case confirmPassword
        case button
    }
    
    enum PasswordActiveProtocol: Int {
        case forgot
        case change
    }
    
    // Set default protocol to SignUp
    var activeProtocol:PasswordActiveProtocol = .change
    
    var emailTextFieldCell:TextFieldCell?
    var oldPasswordTextFieldCell:TextFieldCell?
    var passwordTextFieldCell:TextFieldCell?
    var confirmPasswordTextFieldCell:TextFieldCell?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.tintColor = .white
        loadTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func needsNavigationBarTransparent() -> Bool {
        return true
    }
    
    func loadTableView() {
        let backgroundImageView = UIImageView (image: UIImage(named: "Background_Login"))
        backgroundImageView.contentMode = .scaleAspectFill
        tableView.backgroundView = backgroundImageView
        tableView.register(UINib(nibName: "TextFieldCell", bundle: nil), forCellReuseIdentifier: "TextFieldCell")
        tableView.register(UINib(nibName: "ButtonCell", bundle: nil), forCellReuseIdentifier: "ButtonCell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.allowsSelection = false
    }
    
    func cellList() -> [PasswordCellType] {
        switch self.activeProtocol {
        case .change:
            return [.topSeparator, .oldPassword, .password, .confirmPassword, .smallSeparator, .button]
        case .forgot:
            return [.topSeparator, .email, .smallSeparator, .button]
        }
    }
    
    // MARK: - Actions and Selectors
    
    func actionButtonTapped () {
        switch activeProtocol {
        case .change:
            if (!validateForChange()) {
                return
            }
            changePassword()
            break
        case .forgot:
            if (!validateForForgot()) {
                return
            }
            userForgotPassword()
            break
        }
    }
    
    func userForgotPassword () {
        if let email = emailTextFieldCell?.textField.text {
            AppManager.shared.showHUD(self.view, message: localizedStringForKey("loading"))
            PanaAPI.sharedAPI.forgotPasswordFor(email, successBlock: { (responseDict) in
                AppManager.shared.hideHUD(self.view)
                if let title = responseDict["title"], let message = responseDict["body"] {
                    self.showAlertSingleOption(title, message: message)
                    self.resetTextFields()
                }
            }, errorBlock: { (error) in
                AppManager.shared.hideHUD(self.view)
                if let errorDict = error?["errors"] as? [String:AnyObject] , let message = errorDict["message"] as? String {
                    self.showAlertSingleOption(localizedStringForKey("sorry"), message: message)
                }
            })
        }
    }
    
    func changePassword () {
        if let oldPassword = oldPasswordTextFieldCell?.textField.text, let newPassword = passwordTextFieldCell?.textField.text {
            AppManager.shared.showHUD(self.view, message: localizedStringForKey("loading"))
            PanaAPI.sharedAPI.changePassword(oldPassword, newPassword: newPassword, successBlock: { (response) in
                AppManager.shared.hideHUD(self.view)
                if let title = response["title"], let message = response["body"] {
                    self.showAlertSingleOption(title, message: message)
                    self.resetTextFields()
                }
            }, errorBlock: { (error) in
                AppManager.shared.hideHUD(self.view)
                if let error = error?["errors"] as? [String:AnyObject], let messages = error["message"]?["old_password"] as? [String]   {
                    self.showAlertSingleOption(localizedStringForKey("sorry"), message: messages[0])
                } else {
                    self.showAlertSingleOption(localizedStringForKey("sorry"), message: localizedStringForKey("general_error_message"))
                }
            })
        }
    }
    
    // MARK: - Helpers
    
    func resetTextFields () {
        self.oldPasswordTextFieldCell?.textField.text = nil
        self.passwordTextFieldCell?.textField.text = nil
        self.confirmPasswordTextFieldCell?.textField .text = nil
        self.emailTextFieldCell?.textField.text = nil
    }

    
    // MARK: - Validation Methods

    func validateForForgot() -> Bool {
        
        var isValidated:Bool = true
        
        if (!emailTextFieldCell!.hasText() || !emailTextFieldCell!.isEmailTextValid()) {
            isValidated = false
            emailTextFieldCell!.needsToDisplayError(true)
        } else {
            emailTextFieldCell!.needsToDisplayError(false)
        }
        
        return isValidated
    }
    
    func validateForChange() -> Bool {
        var isValidated:Bool = true
        
        if (!oldPasswordTextFieldCell!.hasText()) {
            isValidated = false
            oldPasswordTextFieldCell!.needsToDisplayError(true)
            return isValidated
        } else {
            oldPasswordTextFieldCell!.needsToDisplayError(false)
        }
        
        if (!passwordTextFieldCell!.hasText()) {
            isValidated = false
            passwordTextFieldCell!.needsToDisplayError(true)
            return isValidated
        } else {
            passwordTextFieldCell!.needsToDisplayError(false)
        }
        
        if (activeProtocol == .change) {
            if (passwordTextFieldCell!.textField.text!.characters.count < 8 ) {
                showAlertSingleOption(localizedStringForKey("sorry"), message: localizedStringForKey("password_length_alert"))
                isValidated = false
            }
            if (passwordTextFieldCell!.textField.text != confirmPasswordTextFieldCell!.textField.text) || (!passwordTextFieldCell!.isNoSpacesValid()) {
                isValidated = false
                passwordTextFieldCell!.needsToDisplayError(true)
                confirmPasswordTextFieldCell!.needsToDisplayError(true)
            } else {
                passwordTextFieldCell!.needsToDisplayError(false)
                confirmPasswordTextFieldCell!.needsToDisplayError(false)
            }
        }
        
        return isValidated
    }

    // MARK: - UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellList().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType:PasswordCellType = cellList()[indexPath.row]
        
        switch cellType {
        case .topSeparator:
            let cell = UITableViewCell ()
            cell.backgroundColor = .clear
            return cell
        case .email:
            let cell: TextFieldCell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.placeholder = localizedStringForKey("email")
            cell.textField.keyboardType = .emailAddress
            cell.textField.autocapitalizationType = .none
            cell.textField.returnKeyType = UIReturnKeyType.send
            cell.textField.delegate = self
            cell.applyCornerRadiusToAllCorners()
            cell.shouldHideUnderLineView = true
            cell.imgView.image = UIImage (named: "Email_Image")
            emailTextFieldCell = cell
            return cell
        case .oldPassword:
            let cell: TextFieldCell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.placeholder = localizedStringForKey("old_password")
            cell.textField.isSecureTextEntry = true
            cell.textField.returnKeyType = UIReturnKeyType.done
            cell.textField.delegate = self
            cell.shouldApplyTopCornerRadius = true
            cell.imgView.image = UIImage (named: "Password_Image")
            oldPasswordTextFieldCell = cell
            return cell
        case .password:
            let cell: TextFieldCell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.placeholder = localizedStringForKey("new_password")
            cell.textField.isSecureTextEntry = true
            cell.textField.returnKeyType = UIReturnKeyType.done
            cell.textField.delegate = self
            cell.imgView.image = UIImage (named: "Password_Image")
            passwordTextFieldCell = cell
            return cell
        case .confirmPassword:
            let cell: TextFieldCell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
            cell.textField.placeholder = localizedStringForKey("confirm_password")
            cell.textField.isSecureTextEntry = true
            cell.textField.returnKeyType = UIReturnKeyType.send
            cell.textField.delegate = self
            cell.shouldApplyBottomCornerRadius = true
            cell.shouldHideUnderLineView = true
            cell.imgView.image = UIImage (named: "Password_Image")
            confirmPasswordTextFieldCell = cell
            return cell
        case .button:
            let cell: ButtonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell", for: indexPath) as! ButtonCell
            cell.btn.addTarget(self, action: #selector(self.actionButtonTapped), for: .touchUpInside)
            cell.setupWith(title: activeProtocol == .change ? localizedStringForKey("change_password") : localizedStringForKey("send"))
            cell.btn.titleLabel?.font = AppTheme.fontForThemeElement(.loginButton)
            return cell
        case .smallSeparator:
            let cell = UITableViewCell ()
            cell.backgroundColor = .clear
            return cell
        }
    }
    
    // MARK: - UITableViewDelegate methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellType:PasswordCellType = self.cellList()[indexPath.row]
        
        switch cellType {
        case .button:
            return ButtonCell.preferredHeight()
        case .topSeparator:
            return 80
        case .smallSeparator:
            return 30
        case .email, .oldPassword, .password, .confirmPassword:
            return TextFieldCell.preferredHeight()
        }
    }
    
    
    // MARK: - UITextViewDelegate methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (activeProtocol == .change && textField == confirmPasswordTextFieldCell?.textField) || (activeProtocol == .forgot && textField == emailTextFieldCell?.textField) {
            hideKeyboard()
            actionButtonTapped()
            return true
        }
        hideKeyboard()
        return true
    }
}

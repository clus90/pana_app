//
//  APIModules.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 8/16/16.
//  Copyright © 2016 Sappito Technologies, C.A. All rights reserved.
//

import UIKit
import CoreLocation
import AFNetworking
import MercadoPagoSDK

extension PanaAPI {
    
    func loginWithUsername(_ username: String, password: String, successBlock:@escaping () -> (), errorBlock: @escaping (_ error: AnyObject?) -> ()) {
        let parameters:[String:String] = Router.parametersForLogin(username, password: password)
        let request = PanaAPIRequest (service: PanaAPIRequest.APIService.login, parameters: parameters as AnyObject?, startNode: nil)
        self.requestWithRequest(request, pathArgs: nil, completion: { (response) in
            if let dict = response as? [String:AnyObject], let accessToken = dict["access_token"] as? String, let refreshToken = dict["refresh_token"] as? String {
                AppManager.shared.userDidLoginWhithCredentials(accessToken: accessToken, refreshToken: refreshToken, email: username, password: password)
                successBlock ()
            } else {
                errorBlock (nil)
            }
        }) { (error) in
            errorBlock (error)
        }
    }
    
    func registerUser (_ firstName: String, lastName: String, email: String, countryCode: String, phone: String, idType: String, id: String, password: String,  successBlock:@escaping () -> (), errorBlock: @escaping (_ error: AnyObject?) -> ()) {
        let parameters:[String:String] = Router.parametersForRegistration(firstName, lastName: lastName, email: email, countryCode: countryCode, phone: phone, idType: idType, id: id, password: password)
        let request = PanaAPIRequest (service: PanaAPIRequest.APIService.register, parameters: parameters as AnyObject?, startNode: "user")
        self.requestWithRequest(request, pathArgs: nil, completion: { (response) in
            if let _ = response as? [String:AnyObject] {
                successBlock ()
            } else {
                errorBlock (nil)
            }
        }) { (error) in
            print ("error registerin: \(error)")
            errorBlock (error)
        }
    }
    
    func forgotPasswordFor (_ email: String, successBlock:@escaping (_ response: [String:String]) -> (), errorBlock: @escaping (_ error: AnyObject?) -> ()) {
        let params = ["email": email]
        let request = PanaAPIRequest (service: .forgotPassword, parameters: params as AnyObject?, startNode: "message")
        self.requestWithRequest(request, pathArgs: nil, completion: { (response) in
            if let response = response as? [String:String] {
                successBlock (response)
            } else {
                errorBlock (nil)
            }
        }) { (error) in
            errorBlock (error)
        }
    }
    
    func updateAccessToken(_ refreshToken: String, successBlock:@escaping (_ token: String, _ refreshToken: String) -> (), errorBlock: @escaping (_ error: AnyObject?) -> ()) {
        let parameters:[String:String] = Router.parameterForAccessTokenUpdate (refreshToken)
        let request = PanaAPIRequest (service: PanaAPIRequest.APIService.updateAccessToken, parameters: parameters as AnyObject?, startNode: nil)
        self.requestWithRequest(request, pathArgs: nil, completion: { (response) in
            if let dict = response as? [String:AnyObject], let accessToken = dict["access_token"] as? String, let newRefreshToken = dict["refresh_token"] as? String {
                successBlock (accessToken, newRefreshToken)
            } else {
                errorBlock (nil)
            }
        }) { (error) in
            if let error = error as? AFHTTPRequestOperation {
                errorBlock (error.response?.statusCode as AnyObject)
                return
            }
            errorBlock (error)
        }
    }
    
    func getProfile(successBlock:@escaping (_ user: User) -> (), errorBlock: @escaping (_ error: AnyObject?) -> ()) {
        let parameters:[String:AnyObject] = Router.parameterAccessToken()
        let request = PanaAPIRequest (service: PanaAPIRequest.APIService.getProfile, parameters: parameters as AnyObject?, startNode: "user")
        requestWithRequest(request, pathArgs: nil, parseClass: User.self, completion: { (user) in
            if let user = user as? User {
                successBlock (user)
            } else {
                errorBlock (nil)
            }
        }) { (error) in
            if let error = error as? AFHTTPRequestOperation {
                errorBlock (error.response?.statusCode as AnyObject)
                return
            }
            errorBlock (error)
        }
    }
    
    func updateProfile(_ user: User, successBlock:@escaping () -> (), errorBlock: @escaping (_ error: AnyObject?) -> ()) {
        let parameters:[String:String] = Router.parametersForUpdateProfile(user)
        let request = PanaAPIRequest (service: PanaAPIRequest.APIService.updateProfile, parameters: parameters as AnyObject?, startNode: nil)
        requestWithRequest(request, pathArgs: nil, completion: { (response) in
            successBlock()
        }) { (error) in
            errorBlock (error)
        }
        
    }
    
    func changePassword (_ oldPassword: String, newPassword: String, successBlock:@escaping (_ response: [String:String]) -> (), errorBlock: @escaping (_ error: AnyObject?) -> ()) {
        let params = Router.parametersForUpdatePassword(oldPassword, newPassword: newPassword)
        let request =  PanaAPIRequest (service: .changePassword, parameters: params as AnyObject?, startNode: nil)
        requestWithRequest(request, pathArgs: nil, completion: { (response) in
            if let error = response?["errors"] as? [String:AnyObject] {
                errorBlock (error as AnyObject?)
            } else if let response = response? ["message"] as? [String:String] {
                successBlock (response)
            }
        }) { (error) in
            errorBlock (error)
        }
    }
    
    func requestService (_ coordinage: CLLocationCoordinate2D,googleRef: String, refPoint: String?, successBlock:@escaping () -> (), errorBlock: @escaping (_ error: AnyObject?) -> ()) {
        let parameters:[String:AnyObject] = Router.parameterForServiceRequest(coordinage, googleRef: googleRef, refPoint: refPoint)
        print ("Params: \(parameters)")
        let request = PanaAPIRequest (service: PanaAPIRequest.APIService.requestService, parameters: parameters as AnyObject?, startNode: "petition")
        self.requestWithRequest(request, pathArgs: nil, parseClass: Service.self, completion: { (response) in
            if let response = response as? Service {
                self.getServiceInfo(Int(response.id), successBlock: { (service) in
                    AppManager.shared.setService(response)
                    successBlock ()
                    }, errorBlock: { (error) in
                        errorBlock (error)
                })
            }
            }) { (error) in
                errorBlock (error)
        }
    }
    
    func getServiceInfo (_ id: Int, successBlock:@escaping (_ service: Service) -> (), errorBlock: @escaping (_ error: AnyObject?) -> ()) {
        let apiRequest = PanaAPIRequest (service: PanaAPIRequest.APIService.getServiceInfo(id: "\(id)"), parameters: nil, startNode: "service")
        self.requestWithRequest(apiRequest, pathArgs: nil, parseClass: Service.self, completion: { (response) in
            if let response = response as? Service {
                successBlock (response)
            }
        }) { (error) in
            errorBlock (error)
        }
    }
    
    func endService (_ id: Int, successBlock:@escaping () -> (), errorBlock: @escaping (_ error: AnyObject?) -> ()) {
        let request = PanaAPIRequest (service: PanaAPIRequest.APIService.endService(id: "\(id)"), parameters: nil, startNode: nil)
        self.requestWithRequest(request, pathArgs: nil, completion: { (response) in
            successBlock ()
            }) { (error) in
                errorBlock (error)
        }
    }
    
    func getCancelingReasons ( successBlock:@escaping (_ reasons: [CancelReason]) -> (), errorBlock: @escaping (_ error: AnyObject?) -> ()) {
        let request = PanaAPIRequest (service: PanaAPIRequest.APIService.getCancelingReasons, parameters: nil, startNode: "canceled_reasons")
        self.requestWithRequest(request, pathArgs: nil, parseClassArray: CancelReason.self, completion: { (reasons) in
            if let reasons = reasons as? [CancelReason] {
                successBlock (reasons)
            } else {
                errorBlock(nil)
            }
        }) { (error) in
            errorBlock (error)
        }
    }
    
    func cancelService (_ id: Int, reason: CancelReason, successBlock:@escaping () -> (), errorBlock: @escaping (_ error: AnyObject?) -> ()) {
        let params = Router.parameterForCancelingService(reason: reason)
        let request = PanaAPIRequest (service: PanaAPIRequest.APIService.cancelService(id: "\(id)"), parameters: params as AnyObject?, startNode: nil)
        self.requestWithRequest(request, pathArgs: nil, completion: { (response) in
            successBlock ()
        }) { (error) in
            errorBlock (error)
        }
    }
    
    func getMercadoPagoConfig (successBlock:@escaping (_ config: MercadoPagoConfig) -> (), errorBlock: @escaping (_ error: AnyObject?) -> ()) {
        let request = PanaAPIRequest (service: .mercadopagoConfig, parameters: nil, startNode: "config")
        self.requestWithRequest(request, pathArgs: nil, parseClass: MercadoPagoConfig.self, completion: { (response) in
            if let response = response as? MercadoPagoConfig {
                successBlock (response)
            } else {
                errorBlock (nil)
            }
        }) { (error) in
            errorBlock (error)
        }
    }
    
    func getUserCards ( successBlock:@escaping (_ cards: [Card]) -> (), errorBlock: @escaping (_ error: AnyObject?) -> ()) {
        let params = Router.parameterAccessToken()
        let request = PanaAPIRequest (service: PanaAPIRequest.APIService.getUserCardsMP, parameters: params as AnyObject?, startNode: "cards")
        requestWithRequest(request, pathArgs: nil, completion: { (response) in
            if let cardsJson = response as? [NSDictionary] {
                var cards: [Card] = []
                for cardDict in cardsJson {
                    let card = Card.fromJSON(cardDict)
                    cards.append(card)
                }
                print ("Hasta aca bien")
                successBlock(cards)
                return
            }
            successBlock([])
            return
        }) { (error) in
            print ("Error: \(error)")
            errorBlock (error)
        }
    }
    
    func submitPayment (token: String, amount: String, isNewCard: Bool, paymentMethod: String, successBlock:@escaping (_ message: String) -> (), errorBlock: @escaping (_ error: AnyObject?) -> ()) {
        let params = Router.parametersForSubmitPayment(token: token, amount: amount, isNewCard: isNewCard, paymentMethod: paymentMethod)
        let request = PanaAPIRequest (service: .submintPayment, parameters: params as AnyObject?, startNode: "message")
        requestWithRequest(request, pathArgs: nil, completion: { (response) in
            if let message = response as? String {
                successBlock (message)
                return
            }
            errorBlock (nil)
        }) { (error) in
            errorBlock (error)
        }
    }
    
}

//
//  Router.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 8/16/16.
//  Copyright © 2016 Sappito Technologies, C.A. All rights reserved.
//

import UIKit
import CoreLocation

class Router: NSObject {
    
    class func parameterAccessToken () -> [String:AnyObject] {
        return ["access_token": AppManager.shared.sessionToken() as AnyObject]
    }
    
    class func parametersForLogin(_ username: String, password: String) -> [String : String] {
        return ["username":username,
                "password":password,
                "grant_type": "password",
                "client_id": kClient_Id,
                "client_secret": kClient_Secret]
    }
    
    class func parametersForRegistration(_ firstName: String, lastName: String, email: String,countryCode: String, phone: String, idType: String, id: String, password: String) -> [String : String] {
        return ["first_name":firstName,
                "last_name":lastName,
                "email": email,
                "call_code": countryCode,
                "phone": phone,
                "type_document": idType,
                "document": id,
                "password": password]
    }
    
    class func parameterForAccessTokenUpdate (_ refreshToken: String) -> [String:String] {
        return ["refresh_token":refreshToken,
                "grant_type": "refresh_token",
                "client_id": kClient_Id,
                "client_secret": kClient_Secret]
    }
    
    class func parametersForUpdateProfile(_ user: User) -> [String : String] {
        return ["access_token": AppManager.shared.sessionToken(),
                "first_name":user.firstName,
                "last_name":user.lastName,
                "email": user.email,
                "call_code": user.countryCode,
                "phone": user.phone,
                "type_document": user.documentType,
                "document": user.document]
    }
    
    class func parametersForUpdatePassword(_ oldPassword: String, newPassword: String) -> [String : String] {
        return ["access_token": AppManager.shared.sessionToken(),
                "old_password":oldPassword,
                "password": newPassword]
    }
    
    class func parametersForSubmitPayment (token: String, amount: String, isNewCard: Bool, paymentMethod: String) -> [String: AnyObject] {
        return ["access_token" : AppManager.shared.sessionToken() as AnyObject,
                "token"       	: token as AnyObject,
                "description" 	: "Pago Servicio Pana" as AnyObject,
                "amount"      	: amount as AnyObject,
                "is_new_card" 	: isNewCard as AnyObject,
                "payment_method"  : paymentMethod as AnyObject]
    }
    
    class func parameterForCancelingService (reason:CancelReason) -> [String:AnyObject] {
        return ["canceled_reason_id": reason.id as AnyObject]
    }
    
    class func parameterForServiceRequest (_ coordinate: CLLocationCoordinate2D, googleRef: String, refPoint: String?) -> [String:AnyObject] {
        var params: [String:AnyObject] =  ["access_token": AppManager.shared.sessionToken() as AnyObject,
                                          "latitude" : coordinate.latitude as AnyObject,
                                          "longitude": coordinate.longitude as AnyObject,
                                           "google_reference_point": googleRef as AnyObject]
        if let refPoint = refPoint {
            params["reference_point"] = refPoint as AnyObject?
        }
        return params
        
    }
    
}

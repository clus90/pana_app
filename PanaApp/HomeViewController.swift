//
//  HomeViewController.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 7/9/16.
//  Copyright © 2016 Sappito Technologies, C.A. All rights reserved.
//

import UIKit
import GoogleMaps
import MercadoPagoSDK

class HomeViewController: BaseViewController {
    
    var mapView = GMSMapView ()
    var referenceTF = UITextField ()
    var isMapSet = false
    var pinCoordinate: CLLocationCoordinate2D?
    var currentAddressLabel = UILabel ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerForNotifications()
        self.setupHomeView ()
        self.setupMap()
        self.getUserInfo ()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AppManager.shared.getUser(completion: nil)
    }
    
    override func needsMenuBarButton() -> Bool {
        return true
    }
    
    // MARK: - Notifications
    
    func registerForNotifications () {
        NotificationCenter.default.addObserver(self, selector: #selector(self.locationManagerObserver), name: NSNotification.Name(rawValue: "didUpdateLocation"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.centerMapCameraOnLocation), name: NSNotification.Name(rawValue: "appDidBecomeActive"), object: nil)
    }
    
    // MARK: - Setup
    
    func setupHomeView () {
        let submitButton = UIButton ()
        submitButton.setTitle(localizedStringForKey("requestService"), for: UIControlState())
        submitButton.setTitleColor(AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.homeButton), for: UIControlState())
        submitButton.backgroundColor = AppTheme.backgroundColorForThemeElement(AppTheme.AppThemeElement.homeButton)
        submitButton.layer.cornerRadius = 5
        submitButton.addTarget(self, action: #selector(self.submitButtonTapped(_:)), for: UIControlEvents.touchUpInside)
        self.addShadowToView(submitButton)
        
        let topContainerView = UIView ()
        topContainerView.backgroundColor = AppTheme.backgroundColorForThemeElement(AppTheme.AppThemeElement.textField)
        topContainerView.layer.borderColor = AppTheme.backgroundColorForThemeElement(AppTheme.AppThemeElement.textFieldBorder).cgColor
        topContainerView.layer.borderWidth = 1
        topContainerView.layer.cornerRadius = 5
        self.addShadowToView(topContainerView)
        
        let pencilImageView = UIImageView (image: UIImage(named: "Pencil_Image"))
        pencilImageView.contentMode = UIViewContentMode.scaleAspectFill
        
        let horizontalSeparatorLineView = UIView ()
        horizontalSeparatorLineView.backgroundColor = AppTheme.backgroundColorForThemeElement(AppTheme.AppThemeElement.separatorLine)
        
        self.currentAddressLabel.textColor = AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.homeAddress)
        self.currentAddressLabel.font = AppTheme.fontForThemeElement(AppTheme.AppThemeElement.homeAddress)
        
        self.referenceTF.placeholder = localizedStringForKey("referencePoint")
        self.referenceTF.clearButtonMode = UITextFieldViewMode.whileEditing
        self.referenceTF.textColor = AppTheme.textColorForThemeElement(AppTheme.AppThemeElement.textField)
        
        let pinImageView = UIImageView (image: UIImage (named: "Map_Pin"))
        pinImageView.contentMode = .scaleAspectFit
        
        self.view.addSubview(self.mapView)
        self.view.addSubview(submitButton)
        self.view.addSubview(topContainerView)
        self.view.addSubview(pinImageView)
        topContainerView.addSubview(pencilImageView)
        topContainerView.addSubview(horizontalSeparatorLineView)
        topContainerView.addSubview(self.referenceTF)
        topContainerView.addSubview(self.currentAddressLabel)
        
        self.mapView.snp.makeConstraints { (make) in
            make.edges.equalTo(self.view)
        }
        
        pinImageView.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.view).offset(-52)
            make.centerX.equalTo(self.view).offset(-22)
            make.size.equalTo(50)
        }
        
        topContainerView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view).offset(20)
            make.left.equalTo(self.view).offset(20)
            make.right.equalTo(self.view).offset(-20)
            make.height.equalTo(70)
        }
        
        pencilImageView.snp.makeConstraints { (make) in
            make.top.equalTo(topContainerView).offset(8)
            make.left.equalTo(topContainerView).offset(20)
            make.size.equalTo(25)
        }
        
        self.referenceTF.snp.makeConstraints { (make) in
            make.top.equalTo(topContainerView).offset(3)
            make.left.equalTo(pencilImageView.snp.right).offset(20)
            make.height.equalTo(35)
            make.right.equalTo(topContainerView).offset(-5)
        }
        
        horizontalSeparatorLineView.snp.makeConstraints { (make) in
            make.top.equalTo(self.referenceTF.snp.bottom).offset(2)
            make.left.equalTo(topContainerView).offset(10)
            make.right.equalTo(topContainerView).offset(-10)
            make.height.equalTo(1)
        }
        
        self.currentAddressLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(topContainerView)
            make.bottom.equalTo(topContainerView).offset(-5)
        }
        
        submitButton.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.view).offset(-20)
            make.left.equalTo(self.view).offset(20)
            make.right.equalTo(self.view).offset(-20)
            make.height.equalTo(50)
        }
        
    }
    
    func addShadowToView (_ view: UIView) {
        view.layer.shadowColor = AppTheme.backgroundColorForThemeElement(AppTheme.AppThemeElement.shadow).cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: -10, height: 10)
        view.layer.shadowRadius = 15
    }
    
    // MARK: - Map Methods
    
    func setupMap () {
        if let coordinate = LocationManager.sharedManager.currentLocation {
            self.mapView.camera = GMSCameraPosition(target: coordinate, zoom: 4, bearing: 0, viewingAngle: 0)
            self.mapView.settings.myLocationButton = true
            self.mapView.isMyLocationEnabled = true
            self.mapView.padding = UIEdgeInsets (top: 0, left: 0, bottom: 70, right: 0)
            self.mapView.delegate = self
            self.isMapSet = true
            delay(seconds: 2.5, completion: {
                let zoomIn = GMSCameraUpdate.zoom(to: 16)
                self.mapView.animate(with: zoomIn)
            })
        }
    }
    
    // MARK: - Selectors
    
    func locationManagerObserver () {
        if !self.isMapSet {
            self.setupMap()
        }
    }
    
    func centerMapCameraOnLocation () {
        if let coordinate = LocationManager.sharedManager.currentLocation {
            self.mapView.camera = GMSCameraPosition (target: coordinate, zoom: 16, bearing: 0, viewingAngle: 0)
        }
        self.getUserInfo()
    }
    
    func submitButtonTapped (_ sender: UIButton) {        
        if let coordinate = self.pinCoordinate {
            if let user = AppManager.shared.user {
                if !user.mercadoPago {
                    self.presentModalView(.mp_Main, completion: nil)
                    return
                }
                if user.status {
                    if Reachability.isConnectedToNetwork() {
                        requestService(coordinate: coordinate)
                    } else {
                        self.showCallerAlertWith(message: localizedStringForKey("unableToConnectMessage"))
                    }
                } else {
                    showCallerAlertWith(message: localizedStringForKey("user_inactive_error"))
                }
            } else {
                AppManager.shared.getUser(completion: nil)
                showCallerAlertWith(message: localizedStringForKey("unableToConnectMessage"))
            }
        }
    }
    
    // MARK: - Custom Methods
    
    func getUserInfo () {
        AppManager.shared.getUser {
            if let user = AppManager.shared.user {
                if !user.mercadoPago {
                    self.getMPCards()
                }
            }
        }
    }
    
    func getMPCards () {
        AppManager.shared.showHUD(self.view, message: localizedStringForKey("loading"))
        PanaAPI.sharedAPI.getUserCards(successBlock: { (cards) in
            AppManager.shared.hideHUD(self.view)
            print ("count: \(cards.count)")
            // TODO:
            if cards.count > 1 {
                self.presentModalView(.mp_Cards, completion: { (vc) in
                    if let nav = vc as? UINavigationController, let viewController = nav.topViewController as? MPMainViewController {
                        viewController.loadItemsFrom(cards: cards)
                    }
                })
            } else {
                self.presentModalView(.mp_NewCardController, completion: nil)
            }
        }) { (error) in
            AppManager.shared.hideHUD(self.view)
            self.showAlertSingleOption(localizedStringForKey("sorry"), message: localizedStringForKey("unable_to_get_cards_error"))
        }
    }
    
    func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            if let address = response?.firstResult() {
                let lines = address.lines as [String]!
                self.currentAddressLabel.text = lines?[0]
            }
        }
    }
    
    func requestService (coordinate: CLLocationCoordinate2D) {
        var googleRef = "No hay referencia"
        if let addressText = currentAddressLabel.text {
            googleRef = addressText
        }
        var refPoint: String? = nil
        if referenceTF.hasText {
            refPoint = referenceTF.text
        }
        AppManager.shared.showHUD(self.view, message: localizedStringForKey("requestingService"))
        PanaAPI.sharedAPI.requestService(coordinate, googleRef: googleRef, refPoint: refPoint, successBlock: {
            print ("back home")
            AppManager.shared.hideHUD(self.view)
            self.referenceTF.text = nil
            self.pushToView(ViewFactory.AppView.detail) { (viewController) in
                if let viewController = viewController as? DetailViewController {
                    viewController.pinCoordinate = coordinate
                }
            }
        }, errorBlock: { (error) in
            AppManager.shared.hideHUD(self.view)
            self.showCallerAlertWith(message: localizedStringForKey("unableToConnectMessage"))
        })
    }
    
    func showCallerAlertWith (message: String) {
        let alert = UIAlertController (title: localizedStringForKey("sorry"), message: message, preferredStyle: .alert)
        let callAction = UIAlertAction (title: localizedStringForKey("makeCall"), style: UIAlertActionStyle.default, handler: { (action) in
            self.callOperationCenter ()
        })
        let cancelAction = UIAlertAction (title: localizedStringForKey("cancel"), style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(callAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
}

// MARK: - GoogleMaps

extension HomeViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        self.hideKeyboard()
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.pinCoordinate = position.target
        self.reverseGeocodeCoordinate(position.target)
    }
}

//
//  AppManager.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 7/7/16.
//  Copyright © 2016 Sappito Technologies, C.A. All rights reserved.
//

import UIKit
import SAMKeychain
import MMDrawerController
import NVActivityIndicatorView
import MBProgressHUD
import CoreLocation

class AppManager: NSObject, UITabBarControllerDelegate {
    
    enum AuthMode:Int {
        case none = 1
        case logged = 0
    }
    
    let kAuthModeKey:String = "authMode"
    let kRefreshToken_Key:String = "Refresh_Token"
    let kAccessToken_Key = "Access_Token"
    let kService_Key = "Service"
    let kEmail_Key = "Email"
    let kKeychainAuthenticationService:String = "PanaAppBaseService"
    
    var rootViewController: MMDrawerController!
    var currentService: Service?
    var user: User?
    
    static let shared = AppManager()
    
    fileprivate var authToken:String?
    
    //This prevents others from using the default '()' initializer for this class.
    fileprivate override init() {
        
        super.init()
        
        UserDefaults.standard.register(defaults: [kAuthModeKey:AuthMode.none.rawValue])
        
        let loginVC:UINavigationController = ViewFactory.viewControllerForAppView(ViewFactory.AppView.login) as! UINavigationController
        let homeVC: UINavigationController = ViewFactory.viewControllerForAppView(ViewFactory.AppView.home) as! UINavigationController
        
        let authMode:AuthMode = AuthMode(rawValue: UserDefaults.standard.integer(forKey: kAuthModeKey))!
        if authMode == .logged {
            LocationManager.sharedManager.locationManager.startUpdatingLocation()
            if let token = UserDefaults.standard.object(forKey: kAccessToken_Key) as? String {
                self.authToken = token
                self.loginUserInBackground(completion: { 
                    self.getUser(completion: nil)
                    self.checkForActiveService()
                })
                /*delay(seconds: 1.0, completion: {
                    
                    self.getUser(completion: {
                        self.checkForActiveService()
                    })
                })*/
                
//                self.updateAccessToken (completion: { 
//                    self.checkForActiveService ()
//                })
            }
        }
        self.rootViewController = MMDrawerController (center: (authMode == .none) ? loginVC : homeVC, leftDrawerViewController: ViewFactory.viewControllerForAppView(ViewFactory.AppView.menu))
        self.rootViewController.openDrawerGestureModeMask = MMOpenDrawerGestureMode.custom
        self.rootViewController.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.all
        let bounds = UIScreen.main.bounds
        let width = bounds.size.width
        self.rootViewController.maximumLeftDrawerWidth = width * 0.8
        self.rootViewController.setDrawerVisualStateBlock(MMDrawerVisualState.slideVisualStateBlock())
    }
    
    func loginUserInBackground (completion completionBlock: (() -> Void)?) {
        if let email = getSavedEmail(), let password = SAMKeychain.password(forService: kKeychainAuthenticationService, account: email) {
            PanaAPI.sharedAPI.loginWithUsername(email, password: password, successBlock: {
                print ("Logged in background")
                if let completionBlock = completionBlock {
                    completionBlock ()
                }
            }, errorBlock: { (error) in
                print ("Error login in background")
            })
        }
    }
    
    func userDidLoginWhithCredentials (accessToken: String, refreshToken: String, email: String, password: String ) {
        saveSessionToken(accessToken)
        saveLoginCredentials(refreshToken)
        saveEmail(email)
        SAMKeychain.setPassword(password, forService: kKeychainAuthenticationService, account: email)
    }
    
    func saveSessionToken(_ token:String) {
        authToken = token
        UserDefaults.standard.set(token, forKey: kAccessToken_Key)
        UserDefaults.standard.synchronize()
    }
    
    func saveEmail (_ email:String) {
        UserDefaults.standard.set(email, forKey: kEmail_Key)
    }
    
    func sessionToken() -> String {
        if (authToken != nil) {
            return authToken!
        }
        return ""
    }
    
    func getSavedEmail () -> String? {
        return UserDefaults.standard.value(forKey: kEmail_Key) as? String
    }
    
    func refreshToken () -> String? {
        if let rToken = UserDefaults.standard.value(forKey: kRefreshToken_Key) as? String {
            return rToken
        }
        return nil
    }
    
    func isLogged() -> Bool {
        return (sessionToken() != "")
    }
    
    func getUser (completion completionBlock: (() -> Void)?) {
        PanaAPI.sharedAPI.getProfile(successBlock: { (user) in
            self.user = user
            
            if let completion = completionBlock {
                completion()
            }
        }) { (error) in
                self.updateAccessToken(completion: {
                    self.getUser(completion: completionBlock)
                })
        }
    }
    
    func updateAccessToken (completion completionBlock: (() -> Void)?) {
        if let refreshToken = refreshToken() {
            PanaAPI.sharedAPI.updateAccessToken(refreshToken, successBlock: { (accessToken, refreshToken) in
                print ("token refreshed")
                self.saveSessionToken(accessToken)
                self.saveLoginCredentials(refreshToken)
                if let completionBlock = completionBlock {
                    completionBlock ()
                }
                }, errorBlock: { (error) in
                    print ("Error: \(error)")
                    if let error = error as? Int, error >= 400 && error <= 500 {
                        self.logOut()
                    }
                    
            })
        }
    }
    
    func logOut () {
        self.setAuthMode(AppManager.AuthMode.none)
        self.authToken = nil
        self.currentService = nil
        self.serviceEnded()
        self.invalidateCurrentSession()
    }
    
    func setService (_ service: Service) {
        self.currentService = service
        UserDefaults.standard.set(service.id, forKey: kService_Key)
        UserDefaults.standard.synchronize()
    }
    
    func serviceEnded () {
        UserDefaults.standard.set(nil, forKey: kService_Key)
        UserDefaults.standard.synchronize()
    }
    
    func saveLoginCredentials(_ refreshToken:String) {
        
        setAuthMode(.logged)
        
        UserDefaults.standard.setValue(refreshToken, forKey: kRefreshToken_Key)
        UserDefaults.standard.synchronize()
    }
    
    func setCenterView(_ view:ViewFactory.AppView, setup:((_ viewController:UIViewController) -> ())?) {
        let viewController:UIViewController = ViewFactory.viewControllerForAppView(view)
        
        if (setup != nil) {
            setup!(viewController)
        }
        self.rootViewController.centerViewController = viewController
    }
    
    func toggleMenu() {
        self.rootViewController.toggle(.left, animated: true, completion: nil)
    }
    
    func showHUD(_ view: UIView, message: String) {
        
        let indicatorView = NVActivityIndicatorView (frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        indicatorView.color = AppTheme.backgroundColorForThemeElement(AppTheme.AppThemeElement.appMainColor)
        indicatorView.type = .ballScaleRippleMultiple
        indicatorView.startAnimating()
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification?.customView = indicatorView
        loadingNotification?.mode = .customView
        loadingNotification?.color = UIColor.black
        loadingNotification?.dimBackground = true
        loadingNotification?.labelColor = UIColor.white
        loadingNotification?.labelText = message
    }
    
    func hideHUD(_ view: UIView) {
        MBProgressHUD.hideAllHUDs(for: view, animated: true)
    }
    
    func setAuthMode(_ mode:AuthMode) {
        UserDefaults.standard.set(mode.rawValue, forKey: kAuthModeKey)
        UserDefaults.standard.synchronize()
    }
    
    func authMode() -> AuthMode {
        let authMode:AuthMode = AuthMode(rawValue: UserDefaults.standard.integer(forKey: kAuthModeKey))!
        
        return authMode
    }
    
    func checkForActiveService () {
        if let id = UserDefaults.standard.object(forKey: kService_Key) as? Int {
            PanaAPI.sharedAPI.getServiceInfo(id, successBlock: { (service) in
                self.setService(service)
                if let nav = self.rootViewController.centerViewController as? UINavigationController {
                    if let vc = nav.topViewController as? HomeViewController {
                        vc.pushToView(ViewFactory.AppView.detail, completion: { (viewController) in
                            if let viewController = viewController as? DetailViewController {
                                viewController.pinCoordinate = CLLocationCoordinate2D (latitude: Double(service.latitude)!, longitude: Double(service.longitude)!)
                            }
                        })
                    }
                }
                }, errorBlock: { (error) in
            })
        }
    }
    
    func invalidateCurrentSession() {
        setAuthMode(.none)
        if let email = getSavedEmail() {
            SAMKeychain.deletePassword(forService: kKeychainAuthenticationService, account: email)
        }
        UserDefaults.standard.setValue(nil, forKey: kAccessToken_Key)
        UserDefaults.standard.setValue(nil, forKey: kRefreshToken_Key)
        UserDefaults.standard.setValue(nil, forKey: kEmail_Key)
        UserDefaults.standard.synchronize()
        setCenterView(.login, setup: nil)
    }
    

}

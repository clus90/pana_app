//
//  MPMainViewController.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 3/21/17.
//  Copyright © 2017 Sappito Technologies, C.A. All rights reserved.
//

import UIKit
import SnapKit
import MercadoPagoSDK

class MPMainViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    let paymentTypePicker = UIPickerView ()
    var items : [PaymentMethodRow] = []
    var paymentMethods : [PaymentMethod] = []
    var paymentPreference: PaymentPreference?
    var mercadoPagoConfig = MercadoPagoConfig()
    var protocolType: ProtocolType = .newCard
    var paymentTypes: [String] = [localizedStringForKey("select")]
    var selectedPaymentMethod: PaymentMethod!
    var idTypes: [IdentificationType] = []
    
    enum ProtocolType: Int {
        case cards
        case newCard
        case firstCard
    }
    
    enum CellType: Int {
        case smallSeparator
        case header
        case footer
        case card
        case addNewCard
        case paymentType
        case cardNumber
        case securityCode
        case expirationDate
        case name
        case userID
        case paymentButton
    }
    
    // Cells
    var paymentTypeCell: MPTextFieldCell?
    var cardNumberCell: MPTextFieldCell?
    var securityCodeCell: MPTextFieldCell?
    var expirationDateCell: MPTextFieldCell?
    var nameCell: MPTextFieldCell?
    var userIdCell: MPUserIdCell?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view
        if protocolType != .newCard {
            let cancelButton = UIBarButtonItem (image: UIImage(named: "Cancel_Icon"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.closeButtonTapped))
            self.navigationItem.leftBarButtonItem = cancelButton
        }
        setupTableView()
        getConfig()
        setupPickerView()
    }
    
    func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.allowsSelection = true
        tableView.allowsMultipleSelection = false
        let backgroundImageView = UIImageView (image: UIImage(named: "Background_Login"))
        backgroundImageView.contentMode = .scaleAspectFill
        tableView.backgroundView = backgroundImageView
        let paymentMethodNib = UINib(nibName: "MPPaymentMethodCell", bundle: nil)
        self.tableView.register(paymentMethodNib, forCellReuseIdentifier: "MPPaymentMethodCell")
        self.tableView.register(UINib(nibName: "EmptyViewCell", bundle: nil), forCellReuseIdentifier: "EmptyViewCell")
        self.tableView.register(UINib(nibName: "ButtonCell", bundle: nil), forCellReuseIdentifier: "ButtonCell")
        self.tableView.register(UINib(nibName: "MPTextFieldCell", bundle: nil), forCellReuseIdentifier: "MPTextFieldCell")
        self.tableView.register(UINib(nibName: "MPUserIdCell", bundle: nil), forCellReuseIdentifier: "MPUserIdCell")
    }
    
    func setupPickerView () {
        paymentTypePicker.backgroundColor = UIColor.lightGray
        paymentTypePicker.delegate = self
        paymentTypePicker.dataSource = self
    }
    
    override func needsLogoOnNavBar() -> Bool {
        return true
    }
    
    private func cellList () -> [CellType] {
        var cellArray: [CellType] = [.smallSeparator, .header]
        switch protocolType {
        case .cards:
            for _ in items {
                cellArray += [.card]
            }
            cellArray += [.addNewCard, .footer]
            return cellArray
        case .newCard, .firstCard:
            cellArray += [.cardNumber, .paymentType, .securityCode, .expirationDate, .name, .userID, .footer, .smallSeparator, .paymentButton, .smallSeparator]
            return cellArray
        }
        
    }
    
    // MARK: - Selectors 
    
    func closeButtonTapped () {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func addNewCard () {
        self.pushToView(.mp_NewCard, completion: nil)
    }
    
    func makePaymentButtonTapped () {
        if let cardToken = validateAndGetCardToken() {
            if validateCardToken(cardToken) {
                print ("All Valid!!!")
                let mercadoPago = MercadoPago (publicKey: mercadoPagoConfig.publicKey)
                AppManager.shared.showHUD(self.view, message: localizedStringForKey("loading"))
                mercadoPago.createNewCardToken(cardToken, success: { (token) in
                    if let token = token {
                        self.submitPaymentWith(token: token)
                    }
                }, failure: { (error) in
                    print ("Error getting token")
                    AppManager.shared.hideHUD(self.view)
                })
            }
        }
    }
    
    // MARK: - Helpers and Methods
    
    func getConfig () {
        AppManager.shared.showHUD(self.view, message: localizedStringForKey("loading"))
        PanaAPI.sharedAPI.getMercadoPagoConfig(successBlock: { (config) in
            AppManager.shared.hideHUD(self.view)
            self.mercadoPagoConfig = config
            MercadoPagoContext.setPublicKey(config.publicKey)
            switch self.protocolType {
            case .newCard, .firstCard:
                let preferences = PaymentPreference()
                preferences.excludedPaymentTypeIds = ["debit_card", "ticket", "atm", "prepaid_card"]
                self.paymentPreference = preferences
                self.getPaymentMethods()
                break
            case .cards:
                break
            }
        }) { (error) in
            AppManager.shared.hideHUD(self.view)
            self.getConfig()
        }
    }
    
    func getCards () {
        AppManager.shared.showHUD(self.view, message: localizedStringForKey("loading"))
        PanaAPI.sharedAPI.getUserCards(successBlock: { (cards) in
            AppManager.shared.hideHUD(self.view)
            print ("count: \(cards.count)")
            self.loadItemsFrom(cards: cards)
        }) { (error) in
            AppManager.shared.hideHUD(self.view)
            self.showAlertSingleOption(localizedStringForKey("sorry"), message: localizedStringForKey("unable_to_get_cards_error"))
        }
    }
    
    func getPaymentMethods () {
        MPServicesBuilder.getPaymentMethods({(paymentMethods: [PaymentMethod]?) -> Void in
            print ("Payment Methods")
            if let paymentMethods = paymentMethods {
                if let paymentPreference = self.paymentPreference {
                    var currentPaymentMethods = paymentMethods
                    if let excludedPaymentTypeIds = paymentPreference.excludedPaymentTypeIds, excludedPaymentTypeIds.count > 0 {
                        currentPaymentMethods = currentPaymentMethods.filter({
                            return !(excludedPaymentTypeIds.contains($0.paymentTypeId))
                        })
                    }
                    if let excludedPaymentMethodIds = self.paymentPreference?.excludedPaymentMethodIds, excludedPaymentMethodIds.count > 0 {
                        currentPaymentMethods = currentPaymentMethods.filter({
                            return !(excludedPaymentMethodIds.contains($0._id))
                        })
                    }
                    self.paymentMethods = currentPaymentMethods
                } else {
                    self.paymentMethods = paymentMethods
                }
            }
            for method in self.paymentMethods {
                self.paymentTypes += [method.name]
            }
            self.setupPickerView()
            self.tableView.reloadData()
            self.getIdentificationTypes()
        }, failure: { (error: NSError?) -> Void in
            MercadoPago.showAlertViewWithError(error, nav: self.navigationController)
        })
    }
    
    func getIdentificationTypes () {
        print ("GETTING ID TYPES")
        let mercadoPago = MercadoPago(keyType: MercadoPagoContext.keyType(), key: MercadoPagoContext.keyValue())
        mercadoPago.getIdentificationTypes({(identificationTypes: [IdentificationType]?) -> Void in
            if let identificationTypes = identificationTypes {
                print ("ID TYPES")
                self.idTypes = identificationTypes
            }
            self.tableView.reloadData()
        }, failure: { (error: NSError?) -> Void in
            print ("ID Types Error")
            if error?.code == MercadoPago.ERROR_API_CODE {
                self.tableView.reloadData()
            }
        }
        )
    }

    
    func loadItemsFrom (cards: [Card]) {
        for card in cards {
            let paymentMethodRow = PaymentMethodRow()
            paymentMethodRow.card = card
            paymentMethodRow.label = "\(localizedStringForKey("card_finished_with")) " + card.lastFourDigits!
            paymentMethodRow.icon = card.paymentMethod!._id
            self.items.append(paymentMethodRow)
        }
        self.tableView.reloadData()

    }
    
    func validateAndGetCardToken () -> CardToken? {
        var isValidated = true
        var cardNumber = ""
        var securityCode = ""
        var expirationMonth = 0
        var expirationYear = 0
        var name = ""
        var idType = ""
        var idNumber = ""
        
        if let paymentTypeCell = paymentTypeCell {
            let selectedRow = paymentTypePicker.selectedRow(inComponent: 0)
            if selectedRow == 0 {
                isValidated = false
                paymentTypeCell.needsToDisplayError(true)
            } else {
                paymentTypeCell.needsToDisplayError(false)
                selectedPaymentMethod = paymentMethods[selectedRow - 1]
            }
        }
        
        if let cardNumberCell = cardNumberCell {
            if !cardNumberCell.textField.hasText {
                isValidated = false
                cardNumberCell.needsToDisplayError(true)
            } else {
                cardNumberCell.needsToDisplayError(false)
                if let nmbr = cardNumberCell.getValue() {
                    cardNumber = nmbr
                }
            }
        }
        
        if let securityCodeCell = securityCodeCell {
            if !securityCodeCell.textField.hasText {
                isValidated = false
                securityCodeCell.needsToDisplayError(true)
            } else {
                securityCodeCell.needsToDisplayError(false)
                if let code = securityCodeCell.getValue() {
                    securityCode = code
                }
            }
        }
        
        if let expirationDateCell = expirationDateCell {
            if !expirationDateCell.textField.hasText {
                isValidated = false
                expirationDateCell.needsToDisplayError(true)
            } else {
                expirationDateCell.needsToDisplayError(false)
                if let month = expirationDateCell.getExpirationMonth(), let year = expirationDateCell.getExpirationYear() {
                    expirationMonth = month
                    expirationYear = year
                } else {
                    expirationDateCell.needsToDisplayError(true)
                }
            }
        }
        
        if let nameCell = nameCell {
            if !nameCell.textField.hasText {
                isValidated = false
                nameCell.needsToDisplayError(true)
            } else {
                nameCell.needsToDisplayError(false)
                if let nme = nameCell.getValue() {
                    name = nme
                }
            }
        }
        
        if let userIdCell = userIdCell {
            if !userIdCell.typeTextField.hasText || !userIdCell.documentTextField.hasText {
                isValidated = false
                userIdCell.needsToDisplayError(true)
            } else {
                userIdCell.needsToDisplayError(false)
                if let type = userIdCell.getIdType(), let document = userIdCell.getIdNumber() {
                    idType = type
                    idNumber = document
                }
            }
        }
        
        if isValidated {
            return CardToken (cardNumber: cardNumber, expirationMonth: expirationMonth, expirationYear: expirationYear, securityCode: securityCode, cardholderName: name, docType: idType, docNumber: idNumber)
        }
        return nil
        
    }
    
    func validateCardToken(_ cardToken : CardToken) -> Bool {
        
        var result : Bool = true
        
        // Validate card number
        let errorCardNumber = cardToken.validateCardNumber(selectedPaymentMethod)
        if  errorCardNumber != nil {
            result = false
            cardNumberCell?.needsToDisplayError(true)
        } else {
            cardNumberCell?.needsToDisplayError(false)
        }
        
        // Validate expiry date
        let errorExpiryDate = cardToken.validateExpiryDate()
        if errorExpiryDate != nil {
            result = false
            expirationDateCell?.needsToDisplayError(true)
        } else {
            expirationDateCell?.needsToDisplayError(false)
        }
        
        // Validate card holder name
        let errorCardholder = cardToken.validateCardholderName()
        if errorCardholder != nil {
            result = false
            nameCell?.needsToDisplayError(true)
        } else {
            nameCell?.needsToDisplayError(false)
        }
        
        // Validate identification number
        if let idType = userIdCell?.identificationType {
            let errorIdentificationNumber = cardToken.validateIdentificationNumber(idType)
            if errorIdentificationNumber != nil {
                result = false
                userIdCell?.needsToDisplayError(true)
            } else {
                userIdCell?.needsToDisplayError(false)
            }
        } else {
            result = false
            userIdCell?.needsToDisplayError(true)
        }
        
        let errorIdentificationType = cardToken.validateIdentificationType()
        if errorIdentificationType != nil {
            result = false
            userIdCell?.needsToDisplayError(true)
        } else {
            userIdCell?.needsToDisplayError(false)
        }
        
        return result
        
    }
    
    private func submitPaymentWith (token: Token) {
        if let paymentMethod = self.selectedPaymentMethod {
            PanaAPI.sharedAPI.submitPayment(token: token._id, amount: mercadoPagoConfig.price, isNewCard: true, paymentMethod: paymentMethod._id, successBlock: { (message) in
                AppManager.shared.hideHUD(self.view)
                self.showSuccessAlert(message)
            }, errorBlock: { (error) in
                AppManager.shared.hideHUD(self.view)
                print ("Error submiting payment:")
                if let error = error?["errors"] as? [String:AnyObject], let message = error["message"] as? String {
                    self.showAlertSingleOption(localizedStringForKey("sorry"), message: message)
                } else {
                    self.showAlertSingleOption(localizedStringForKey("sorry"), message: localizedStringForKey("unable_to_process_payment"))
                }
            })
        }
    }
    
    // MARK: - Alerts
    
    func showSuccessAlert (_ message: String) {
        let alert = UIAlertController (title: localizedStringForKey("thanks"), message: message, preferredStyle: UIAlertControllerStyle.alert)
        let action = UIAlertAction (title: localizedStringForKey("accept"), style: .default) { (alert) in
            self.navigationController?.dismiss(animated: true, completion: {
                AppManager.shared.getUser(completion: nil)
            })
        }
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }

    
    // MARK: - UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellList().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch cellList()[indexPath.row] {
        case .header:
            let cell: EmptyViewCell = tableView.dequeueReusableCell(withIdentifier: "EmptyViewCell", for: indexPath) as! EmptyViewCell
            cell.shouldApplyTopCornerRadius = true
            let view = MPPaymentView ()
            view.priceLabel.text = self.mercadoPagoConfig.getPriceFormated()
            view.monthsLabel.text = String (format: localizedStringForKey("subscription_for"), self.mercadoPagoConfig.months)
            cell.setWith(view: view)
            return cell
        case .card:
            let cell : MPPaymentMethodCell = tableView.dequeueReusableCell(withIdentifier: "MPPaymentMethodCell", for: indexPath) as! MPPaymentMethodCell
            
            let paymentRow : PaymentMethodRow = items[indexPath.row - 2]
            cell.setLabel(paymentRow.label!)
            cell.setImageWithName(paymentRow.icon!)
            cell.paymentMethodLabel.textColor = .black
            return cell
        case .footer:
            let cell: EmptyViewCell = tableView.dequeueReusableCell(withIdentifier: "EmptyViewCell", for: indexPath) as! EmptyViewCell
            cell.shouldApplyBottomCornerRadius = true
            cell.shouldApplyTopCornerRadius = false
            let view = MPFooterView ()
            cell.setWith(view: view)
            return cell
        case .addNewCard:
            let cell : MPPaymentMethodCell = tableView.dequeueReusableCell(withIdentifier: "MPPaymentMethodCell", for: indexPath) as! MPPaymentMethodCell
            cell.setLabel(localizedStringForKey("add_new_card"))
            cell.paymentMethodLabel.textColor = AppTheme.backgroundColorForThemeElement(.appMainColor)
            return cell
        case .paymentType:
                let cell : MPTextFieldCell = tableView.dequeueReusableCell(withIdentifier: "MPTextFieldCell", for: indexPath) as! MPTextFieldCell
                cell.setupFor(type: .paymentType)
                cell.textField.inputView = paymentTypePicker
                cell.textField.text = paymentTypes[0]
                cell.textField.clearButtonMode = .never
                guard let paymentCell = self.paymentTypeCell else {
                    self.paymentTypeCell = cell
                    return cell
                }
                let row = paymentTypePicker.selectedRow(inComponent: 0)
                paymentCell.textField.text = paymentTypes[row]
                return paymentCell
        case .cardNumber:
                let cell : MPTextFieldCell = tableView.dequeueReusableCell(withIdentifier: "MPTextFieldCell", for: indexPath) as! MPTextFieldCell
                cell.setupFor(type: .cardNumber)
                cardNumberCell = cell
                return cell
        case .securityCode:
                let cell : MPTextFieldCell = tableView.dequeueReusableCell(withIdentifier: "MPTextFieldCell", for: indexPath) as! MPTextFieldCell
                cell.setupFor(type: .securityCode)
                securityCodeCell = cell
                return cell
        case .expirationDate:
                let cell : MPTextFieldCell = tableView.dequeueReusableCell(withIdentifier: "MPTextFieldCell", for: indexPath) as! MPTextFieldCell
                cell.setupFor(type: .expirationDate)
                expirationDateCell = cell
                return cell
        case .name:
                let cell : MPTextFieldCell = tableView.dequeueReusableCell(withIdentifier: "MPTextFieldCell", for: indexPath) as! MPTextFieldCell
                cell.setupFor(type: .name)
                nameCell = cell
                return cell
        case .userID:
                let cell : MPUserIdCell = tableView.dequeueReusableCell(withIdentifier: "MPUserIdCell", for: indexPath) as! MPUserIdCell
                cell.identificationTypes = self.idTypes
                if self.idTypes.count > 0 {
                    let row = cell.idTypePicker.selectedRow(inComponent: 0)
                    cell.typeSelectedAt(row)
                }
                userIdCell = cell
                return cell
        case .paymentButton:
            let cell: ButtonCell = tableView.dequeueReusableCell(withIdentifier: "ButtonCell", for: indexPath) as! ButtonCell
            cell.btn.addTarget(self, action: #selector(self.makePaymentButtonTapped), for: .touchUpInside)
            cell.setupWith(title: localizedStringForKey("make_payment"))
            cell.btn.titleLabel?.font = AppTheme.fontForThemeElement(.loginButton)
            return cell
        case .smallSeparator:
            let cell = UITableViewCell ()
            cell.backgroundColor = .clear
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    // MARK: - UITableViewDelegate methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch cellList()[indexPath.row] {
        case .header:
            return 170
        case .card, .addNewCard:
            return 50
        case .footer:
            return 140
        case .paymentType, .cardNumber, .securityCode, .expirationDate, .name, .userID:
            return MPTextFieldCell.preferredHeight()
        case .paymentButton:
            return ButtonCell.preferredHeight()
        case .smallSeparator:
            return 30
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch cellList()[indexPath.row] {
        case .card:
            if let card = items[indexPath.row - 2].card {
                self.pushToView(.mp_Confirmation, completion: { (viewController) in
                    if let vc = viewController as? MPPaymentConfirmationViewController {
                        vc.selectedCard = card
                        vc.selectedPaymentMethod = card.paymentMethod
                        vc.mercadoPagoConfig = self.mercadoPagoConfig
                    }
                })
            }
            break
        case .addNewCard:
            self.addNewCard()
            break
        default:
            break
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }

    
}

// MARK: - PickerView Delegate and DataSource

extension MPMainViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return paymentTypes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerView.reloadAllComponents()
        if let textField = paymentTypeCell?.textField {
            textField.text = paymentTypes[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let color = (row == pickerView.selectedRow(inComponent: component)) ? UIColor.green : UIColor.black
        return NSAttributedString(string: paymentTypes[row], attributes: [NSForegroundColorAttributeName: color])
    }
}




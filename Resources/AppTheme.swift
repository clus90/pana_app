//
//  LoginViewController.swift
//  PanaApp
//
//  Created by Carlos Luis Urbina on 7/7/16.
//  Copyright © 2016 Sappito Technologies, C.A. All rights reserved.
//

import UIKit

class AppTheme {

    enum AppThemeElement: Int {
        case appMainColor
        case appSecundaryColor
        case separatorLine
        case textField
        case textFieldBorder
        case loginButton
        case loginLabel
        case homeButton
        case homeAddress
        case menuTitle
        case detailMessage
        case statusTitle
        case callButton
        case cancelButton
        case shadow
    }
    
    class func textColorForThemeElement(_ element:AppThemeElement) -> UIColor{
        
        switch element {
        case .appMainColor:
            return UIColor (netHex: 0x3FE600)
        case .loginLabel, .detailMessage:
            return UIColor.white
        case .loginButton, .homeButton, .callButton, .cancelButton:
            return UIColor.black
        case .menuTitle:
            return AppTheme.backgroundColorForThemeElement(AppTheme.AppThemeElement.appMainColor)
        case .textField, .homeAddress, .statusTitle:
            return UIColor.lightGray
        default:
            return UIColor.yellow
        }
    }
    
    class func backgroundColorForThemeElement(_ element:AppThemeElement) -> UIColor {
        
        switch element {
        case .appMainColor, .homeButton, .loginButton, .callButton:
            return UIColor (netHex: 0x3FE600)
        case .appSecundaryColor, .shadow:
            return UIColor.black
        case .separatorLine:
            return UIColor.lightGray
        case .textField:
            return UIColor.white
        case .textFieldBorder:
            return UIColor(netHex: 0xE5E3E6)
        case .cancelButton:
            return UIColor(netHex: 0xF94A4A)
        default:
            return UIColor.yellow
        }
    }
    
    class func fontForThemeElement(_ element:AppThemeElement) -> UIFont {
        
        switch element {
        case .textField, .loginLabel, .loginButton:
            return UIFont.systemFont(ofSize: 17)
        case .detailMessage, .homeAddress:
            return UIFont.systemFont(ofSize: 15)
        case .statusTitle:
            return UIFont.boldSystemFont(ofSize: 15)
        default:
            return UIFont.systemFont(ofSize: 10)
        }
    }
    
    
}

